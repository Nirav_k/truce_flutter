/**
 * Automatically generated file. DO NOT MODIFY
 */
package netkeepingblog.flutter_ios_voip_kit;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "netkeepingblog.flutter_ios_voip_kit";
  public static final String BUILD_TYPE = "release";
}
