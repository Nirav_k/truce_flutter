class Strings {
  Strings();

  static String appName = 'PHILLY TRUCE';
  static String mobileNo = "+1(267)458-7823";
  static String emailContact = "service@phillytruce.com";

  //Drawer
  static String music = "Music";
  static String videos = "Videos";
  static String news = "Community News";
  static String moreService = "More Services";
  static String contactUs = "Contact Us";
  static String survey = "Philly’s Opinion";
  static String donate = "Donate";
  static String contactUsDescription =
      "Don't hesitate to give us a call or just use the contact form below";
  static String moreServiceDisclaimerText =
      "MENTAL & BEHAVIORAL HEALTH, SUBSTANCE ABUSE, SHELTER & FOOD INSECURITY, CHILDCARE, THERAPY & MORE";
  static String pleaseNote = 'Please Note: ';
  static String moreServiceDisclaimerTextDescription =
      "Filling out this form means you’re asking to be connected to a service provider other than PHILLY TRUCE who’s able to meet a social or medical need that you or someone in your care may have. Once completed, this form is sent to PHILLY TRUCE and someone from PHILLY TRUCE will contact you within two business days. The information you enter is completely confidential and there is no cost for this service. Please use this form only to request services for yourself or a child (under 18 years old) or adult for whom you have legal guardianship. Consent submitted through this form should be signed by the person who would be receiving services or by their parent or legal guardian only.";

  //Home Screen
  static String getHelp = 'Get Help';
  static String becomeAMediator = 'Become a Volunteer';
  // static String mediator = 'Mediator';
  static String mediator = 'Volunteer';

  //User Information
  static String confirmationText = 'Your information will be kept private';
  static String name = 'Name';
  static String firstname = 'First Name';
  static String lastname = 'Last Name';
  static String enterName = 'Enter Name';
  static String enterLastname = 'Enter Lastname';
  static String mobileNumber = 'Mobile Number';
  static String enterMobileNumber = 'Enter Mobile Number';
  static String emailOption = 'Email(optional)';
  static String enterEmailOption = 'Enter Email(optional)';
  static String whereYouAt = 'Where you at?';
  // static String neighborhoodBeef = 'Neighborhood Beef';
  // static String domesticDispute = 'Domestic Dispute';
  // static String other = 'Other';
  // static String moreDetails = 'More details';
  static String personalConflict = 'Personal Conflict';
  static String familyDomesticIssue = "Family/Domestic Issue";
  static String neighborhoodDispute = 'Neighborhood Dispute';
  static String other = 'Other';
  static String whatsGoingOn = 'What\'s going on?';
  static String send = 'Send';
  static String done = 'Done';
  static String cancelRequest = 'Done';
  static String pendingRequest = 'Pending Requests';

  static String dialogMessage = 'Your message has been sent.';
  static String dialogSecondMessage =
      'We got you. Somebody will hit you right back. ';

  static String firstLine = 'WE ARE NOT ANIMALS.';
  static String secondLine = 'MURDER IS NOT AN OPTION.';
  static String thirdLine = 'LET US TALK TO ONE ANOTHER.';
  static String statusPending = 'Done';

  //Mediator
  static String submit = 'Submit';
  static String logIn = 'Login';
  static String enterEmail = 'Enter Email';
  static String enterPassword = 'Enter Password';
  static String enterConfirmPassword = 'Enter Confirm Password';
  static String email = 'Email';
  static String location = 'Location';
  static String password = 'Password';
  static String confirmPassword = 'Confirm Password';
  static String requestBackUp = 'REQUEST BACK-UP';
  static String respond = 'LOCATE USER';
  static String responded = 'RESPONSED';
  static String cancelRequest1 = 'CANCEL REQUEST';
  static String followUp = 'STATUS';
  static String goBackToHome = 'Go Back To Home';
  static String whatStillNeedToHappen1 = 'What happened?';
  static String whatStillNeedToHappen = 'What still needs to happen?';
  static String whatStillNeedToHappenHint = 'Write some thing here...';
  static String userConfirmationText =
      'Your request is pending, Please wait till admin will accept your request';

  static String helpDone = 'This help was done by Main Mediator.';
  static String cancelHelp = 'This help was cancelled by User.';
}
