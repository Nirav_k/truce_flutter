import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

// User Screens
Color whiteColor = const Color(0xffFFFFFF);
Color homeButtonTextColor = const Color(0xff234C93);
Color homeScreenBackgroundColor = const Color(0xff14408C);
Color labelColor = const Color(0xffA5A5A5);
Color sendButtonColor = const Color(0xffB68661);
Color chatReceiverColor = const Color(0xffB9C6DD);
Color trueCallColor = const Color(0xff3EB436);
Color followUpColor = const Color(0xff838383);
Color needHelp = const Color(0xffD47F3F);
Color wantHelp = const Color(0xff2761BF);
Color smallMapCircleColor = const Color.fromRGBO(38, 107, 228, 0.21);
Color bigMapCircleColor = const Color.fromRGBO(38, 107, 228, 0.11);
Color videoBackColor = const Color(0xffB9C6DD);

final fontStyle = 'Oswald';

final registrationHintStyle = TextStyle();

class TimeAgo {
  static String timeAgoSinceDate(String dateString,
      {bool numericDates = true}) {
    DateTime notificationDate =
        DateFormat("yyyy-mm-dd hh:mm:ss").parse(dateString);
    final date2 = DateTime.now();
    final difference = date2.difference(notificationDate);

    if (difference.inDays > 8) {
      return DateFormat("dd-MM-yyyy").format(notificationDate).toString();
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inMinutes >= 1) {
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    } else if (difference.inSeconds >= 3) {
      return '${difference.inSeconds} seconds ago';
    } else {
      return 'Just now';
    }
  }
}

extension StringExtension on String {
  static String displayTimeAgoFromTimestamp(String timestamp) {
    // final year = int.parse(timestamp.substring(0, 4));
    // final month = int.parse(timestamp.substring(5, 7));
    // final day = int.parse(timestamp.substring(8, 10));
    // final hour = int.parse(timestamp.substring(11, 13));
    // final minute = int.parse(timestamp.substring(14, 16));

    if (timestamp.isEmpty || timestamp == null) {
      return "";
    }

    final year = int.parse(DateTime.parse(timestamp).toLocal().year.toString());
    final month =
        int.parse(DateTime.parse(timestamp).toLocal().month.toString());
    final day = int.parse(DateTime.parse(timestamp).toLocal().day.toString());
    final hour = int.parse(DateTime.parse(timestamp).toLocal().hour.toString());
    final minute =
        int.parse(DateTime.parse(timestamp).toLocal().minute.toString());

    final DateTime videoDate = DateTime(year, month, day, hour, minute).toUtc();
    final int diffInHours =
        DateTime.now().toLocal().difference(videoDate).inHours;

    String timeAgo = '';
    String timeUnit = '';
    int timeValue = 0;

    if (diffInHours < 1) {
      final diffInMinutes =
          DateTime.now().toUtc().difference(videoDate).inMinutes;
      timeValue = diffInMinutes;
      if (timeValue == 0) {
        timeUnit = 'Just now';
      } else {
        timeUnit = 'minute';
      }
    } else if (diffInHours < 24) {
      timeValue = diffInHours;
      timeUnit = 'hour';
    } else if (diffInHours >= 24 && diffInHours < 24 * 7) {
      timeValue = (diffInHours / 24).floor();
      timeUnit = 'day';
    } else if (diffInHours >= 24 * 7 && diffInHours < 24 * 30) {
      timeValue = (diffInHours / (24 * 7)).floor();
      timeUnit = 'week';
    } else if (diffInHours >= 24 * 30 && diffInHours < 24 * 12 * 30) {
      timeValue = (diffInHours / (24 * 30)).floor();
      timeUnit = 'month';
    } else {
      timeValue = (diffInHours / (24 * 365)).floor();
      timeUnit = 'year';
    }

    timeAgo = timeValue.toString() + ' ' + timeUnit;
    timeAgo += timeValue > 1 ? 's' : '';

    return timeUnit == "Just now" ? "Just now" : timeAgo + ' ago';
  }
}

Future<dynamic> getDetail(String userUrl) async {
  String embedUrl = "https://www.youtube.com/oembed?url=$userUrl&format=json";

  //store http request response to res variable
  var res = await http.get(embedUrl);

  try {
    if (res.statusCode == 200) {
      //return the json from the response
      return json.decode(res.body);
    } else {
      //return null if status code other than 200
      return null;
    }
  } on FormatException catch (e) {
    print('invalid JSON' + e.toString());
    //return null if error
    return null;
  }
}
