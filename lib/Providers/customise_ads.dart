import 'dart:convert';
import 'dart:io';

import 'package:flutter_app/Model/custmise_ad_list.dart';
import 'package:flutter_app/Utils/CustomException.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'mediator/add_mediator.dart';

class CustomiseAdApiProvider {
  final _baseUrl = '${AddMediatorApiClient.baseUrl}';
  http.Client httpClient = http.Client();

  Future<CustomiseAdList> customiseAdListApi() async {
    final url = '$_baseUrl/Customize_advertise_list';
    print(url);

    var responseJson;
    try {
      final response = await this.httpClient.post(url).timeout(
        Duration(minutes: 1),
        onTimeout: () {
          print('Flutter Fully Truce' + 'time out');
          Fluttertoast.showToast(
              msg: 'Something went wrong . please try again');
          return null;
        },
      );

      print('URL >> ' + url);
      print('RESPONSE >> ${response.body}');
      responseJson = _response(response);
    } on SocketException {
      Fluttertoast.showToast(
          msg:
              'Some thing went Wrong . to update location Please Try again later');
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        {
          return CustomiseAdList.fromJson(json.decode(response.body));
          break;
        }
      case 400:
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode :${response.statusCode}');
    }
  }
}
