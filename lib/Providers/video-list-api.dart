import 'dart:convert';
import 'dart:io';

import 'package:flutter_app/Model/message_list_model.dart';
import 'package:flutter_app/Model/music-list.dart';
import 'package:flutter_app/Model/video_list.dart';
import 'package:flutter_app/Utils/CustomException.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'mediator/add_mediator.dart';

class VideoMusicMessageListAPiClient {
  final _baseUrl = '${AddMediatorApiClient.baseUrl}';
  http.Client httpClient = http.Client();

  Future<VideoList> videoListAPiClient() async {
    final url = '$_baseUrl/Video_list';

    var responseJson;
    try {
      final response = await this.httpClient.post(url).timeout(
        Duration(minutes: 1),
        onTimeout: () {
          print('Flutter Fully Truce' + 'time out');
          Fluttertoast.showToast(
              msg: 'Something went wrong . please try again');
          return null;
        },
      );
      print('url >> ' + url);
      print('Response >> ${response.body}');
      responseJson = _response(response, true, false, false);
    } on SocketException {
      Fluttertoast.showToast(
          msg:
              'Some thing went Wrong . to update location Please Try again later');
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
    /* if (response.statusCode == 200) {
      return response;
    } else {
      print('Fully Truce Location ' + response.body.toString());
      Fluttertoast.showToast(msg: 'Location Something went wrong');
      return null;
    }*/
  }

  Future<MusicList> musicListAPiClient() async {
    final url = '$_baseUrl/Music_list';

    var responseJson;
    try {
      final response = await this.httpClient.post(url).timeout(
        Duration(minutes: 1),
        onTimeout: () {
          print('Flutter Fully Truce' + 'time out');
          Fluttertoast.showToast(
              msg: 'Something went wrong . please try again');
          return null;
        },
      );

      print('API >> ' + url);
      print('RESPONSE >> ${response.body}');
      responseJson = _response(response, false, true, false);
    } on SocketException {
      Fluttertoast.showToast(
          msg:
              'Some thing went Wrong . to update location Please Try again later');
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
    /* if (response.statusCode == 200) {
      return response;
    } else {
      print('Fully Truce Location ' + response.body.toString());
      Fluttertoast.showToast(msg: 'Location Something went wrong');
      return null;
    }*/
  }

  Future<MessageList> messageListAPiClient() async {
    final url = '$_baseUrl/Message_list';

    var responseJson;
    try {
      final response = await this.httpClient.post(url).timeout(
        Duration(minutes: 1),
        onTimeout: () {
          print('Flutter Fully Truce' + 'time out');
          Fluttertoast.showToast(
              msg: 'Something went wrong . please try again');
          return null;
        },
      );

      print('url >> ' + url);
      print('Response >> ${response.body}');
      responseJson = _response(response, false, false, true);
    } on SocketException {
      Fluttertoast.showToast(
          msg:
              'Some thing went Wrong . to update location Please Try again later');
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
    /* if (response.statusCode == 200) {
      return response;
    } else {
      print('Fully Truce Location ' + response.body.toString());
      Fluttertoast.showToast(msg: 'Location Something went wrong');
      return null;
    }*/
  }

  // Future<CustomiseAdList> customiseAdListApi() async {
  //   final url = '$_baseUrl/Customize_advertise_list';
  //
  //   var responseJson;
  //   try {
  //     final response = await this.httpClient.post(url).timeout(
  //       Duration(minutes: 1),
  //       onTimeout: () {
  //         print('Flutter Fully Truce' + 'time out');
  //         Fluttertoast.showToast(
  //             msg: 'Something went wrong . please try again');
  //         return null;
  //       },
  //     );
  //
  //     print('URL >> ' + url);
  //     print('RESPONSE >> ${response.body}');
  //     responseJson = _response(response, false, false, true);
  //   } on SocketException {
  //     Fluttertoast.showToast(
  //         msg:
  //             'Some thing went Wrong . to update location Please Try again later');
  //     throw FetchDataException('No Internet connection');
  //   }
  //   return responseJson;
  // }

  Future<http.Response> messageDeleteAPiClient(bodyData) async {
    final url = '$_baseUrl/delete_admin_message';

    var responseJson;
    try {
      final response = await this.httpClient.post(url, body: bodyData).timeout(
        Duration(minutes: 1),
        onTimeout: () {
          print('Flutter Fully Truce' + 'time out');
          Fluttertoast.showToast(
              msg: 'Something went wrong . please try again');
          return null;
        },
      );

      print('url >> ' + url);
      print('BODy data >> $bodyData');
      print('Response >> ${response.body}');
      responseJson = _response(response, false, false, false);
    } on SocketException {
      Fluttertoast.showToast(
          msg:
              'Some thing went Wrong . to update location Please Try again later');
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(
      http.Response response, bool isVideoList, bool isMusic, bool isMessage) {
    switch (response.statusCode) {
      case 200:
        {
          if (isVideoList)
            return VideoList.fromJson(json.decode(response.body));
          else if (isMusic)
            return MusicList.fromJson(json.decode(response.body));
          else if (isMessage)
            return MessageList.fromJson(json.decode(response.body));
          else
            return response;
          break;
        }
      case 400:
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode :${response.statusCode}');
    }
  }
}
