import 'package:flutter_app/Main/Repository/customise_ad_repository.dart';
import 'package:flutter_app/Model/custmise_ad_list.dart';
import 'package:rxdart/rxdart.dart';

class GetCustomiseAdListBloc {
  final CustomiseAdRepository _repository = CustomiseAdRepository();

  final BehaviorSubject<CustomiseAdList> _subCustomiseAd =
      BehaviorSubject<CustomiseAdList>();

  getCustomiseAdList() async {
    CustomiseAdList customiseAdList = await _repository.getCustomiseAdList();
    subCustomiseAd.sink.add(customiseAdList);
  }

  dispose() {
    _subCustomiseAd.close();
  }

  BehaviorSubject<CustomiseAdList> get subCustomiseAd => _subCustomiseAd;
}

final getCustomiseAdListBloc = GetCustomiseAdListBloc();
