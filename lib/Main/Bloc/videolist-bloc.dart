import 'package:flutter_app/Main/Repository/videolist.dart';
import 'package:flutter_app/Model/message_list_model.dart';
import 'package:flutter_app/Model/music-list.dart';
import 'package:flutter_app/Model/video_list.dart';
import 'package:rxdart/rxdart.dart';

class GetVideoListBloc {
  final VideoListRepository _repository = VideoListRepository();

  final BehaviorSubject<VideoList> _subVideo = BehaviorSubject<VideoList>();
  final BehaviorSubject<MusicList> _subMusic = BehaviorSubject<MusicList>();
  final BehaviorSubject<MessageList> _subMessage =
      BehaviorSubject<MessageList>();
  // final BehaviorSubject<CustomiseAdList> _subCustomiseAd =
  //     BehaviorSubject<CustomiseAdList>();

  getVideoList() async {
    VideoList videoList = await _repository.getVideoList();
    subVideo.sink.add(videoList);
  }

  getMusicList() async {
    MusicList musicList = await _repository.getMusicList();
    subMusic.sink.add(musicList);
  }

  getMessageList() async {
    MessageList messageList = await _repository.getMessageList();
    subMessage.sink.add(messageList);
  }

  // getCustomiseAdList() async {
  //   CustomiseAdList customiseAdList = await _repository.getCustomiseAdList();
  //   subCustomiseAd.sink.add(customiseAdList);
  // }

  dispose() {
    _subVideo.close();
    _subMusic.close();
    _subMessage.close();
    // _subCustomiseAd.close();
  }

  BehaviorSubject<VideoList> get subVideo => _subVideo;
  BehaviorSubject<MusicList> get subMusic => _subMusic;
  BehaviorSubject<MessageList> get subMessage => _subMessage;
  // BehaviorSubject<MessageList> get subCustomiseAd => _subCustomiseAd;
}

final getVideoListBloc = GetVideoListBloc();
