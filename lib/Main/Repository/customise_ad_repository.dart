import 'package:flutter_app/Model/custmise_ad_list.dart';
import 'package:flutter_app/Providers/customise_ads.dart';

class CustomiseAdRepository {
  CustomiseAdApiProvider getCustomiseAd = CustomiseAdApiProvider();

  Future<CustomiseAdList> getCustomiseAdList() async {
    return await getCustomiseAd.customiseAdListApi();
  }
}
