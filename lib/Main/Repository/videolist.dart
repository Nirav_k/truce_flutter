import 'package:flutter_app/Model/message_list_model.dart';
import 'package:flutter_app/Model/music-list.dart';
import 'package:flutter_app/Model/video_list.dart';
import 'package:flutter_app/Providers/video-list-api.dart';

class VideoListRepository {
  VideoMusicMessageListAPiClient getVideoListClient =
      VideoMusicMessageListAPiClient();

  Future<VideoList> getVideoList() async {
    return await getVideoListClient.videoListAPiClient();
  }

  Future<MusicList> getMusicList() async {
    return await getVideoListClient.musicListAPiClient();
  }

  Future<MessageList> getMessageList() async {
    return await getVideoListClient.messageListAPiClient();
  }

  // Future<CustomiseAdList> getCustomiseAdList() async {
  //   return await getVideoListClient.customiseAdListApi();
  // }
}
