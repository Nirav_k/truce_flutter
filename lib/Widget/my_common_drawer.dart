import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/contact_us.dart';
import 'package:flutter_app/Screens/messaging_list.dart';
import 'package:flutter_app/Screens/more_service.dart';
import 'package:flutter_app/Screens/music-screen.dart';
import 'package:flutter_app/Screens/suvey_screen.dart';
import 'package:flutter_app/Screens/videos-screen.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class MyCommonDrawer extends StatelessWidget {
  const MyCommonDrawer({
    Key key,
    @required GlobalKey<ScaffoldState> scaffoldKey,
  })  : _scaffoldKey = scaffoldKey,
        super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: Center(
              child: Image.asset('assets/images/philly_truce_text_logo.png'),
            ),
          ),
          new ListTile(
            title: new Text(
              Strings.videos,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => VideosScreen(),
                  ));
            },
          ),
          new ListTile(
            title: new Text(
              Strings.music,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MusicScreen(),
                  ));
            },
          ),
          new ListTile(
            title: new Text(
              Strings.news,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MessagingScreen(),
                  ));
            },
          ),
          new ListTile(
            title: new Text(
              Strings.survey,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();

              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SurveyFormScreen(),
                  ));
            },
          ),
          new ListTile(
            title: new Text(
              Strings.donate,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              launch(
                  "https://www.paypal.com/donate/?hosted_button_id=7CP5J5GJJXAYN");
            },
          ),
          new ListTile(
            title: new Text(
              Strings.moreService,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MoreService(),
                  ));
            },
          ),
          new ListTile(
            title: new Text(
              Strings.contactUs,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                  fontFamily: fontStyle),
            ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ContactUsScreen(),
                  ));
            },
          ),
          // new ListTile(
          //   title: new Text(
          //     "clear Data",
          //     style: TextStyle(
          //         color: Colors.black,
          //         fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
          //         fontFamily: fontStyle),
          //   ),
          //   onTap: () async {
          //     _scaffoldKey.currentState.openEndDrawer();
          //     SharedPreferences prefs = await SharedPreferences.getInstance();
          //     await prefs.clear();
          //   },
          // ),
        ],
      ),
    );
  }
}
