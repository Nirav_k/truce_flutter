class CustomiseAdList {
  bool status;
  dynamic statusCode;
  dynamic message;
  List<Data> data;

  CustomiseAdList({this.status, this.statusCode, this.message, this.data});

  CustomiseAdList.fromJson(dynamic json) {
    status = json["status"];
    statusCode = json["statusCode"];
    message = json["message"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["statusCode"] = statusCode;
    map["message"] = message;
    if (data != null) {
      map["data"] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  dynamic googleAdvertiseId;
  dynamic googleAdvertiseName;
  dynamic googleAdvertiseImage;
  dynamic googleAdvertiseUrl;
  dynamic googleAdvertiseStatus;
  dynamic createdDate;

  Data(
      {this.googleAdvertiseId,
      this.googleAdvertiseName,
      this.googleAdvertiseImage,
      this.googleAdvertiseUrl,
      this.googleAdvertiseStatus,
      this.createdDate});

  Data.fromJson(dynamic json) {
    googleAdvertiseId = json["google_advertise_id"];
    googleAdvertiseName = json["google_advertise_name"];
    googleAdvertiseImage = json["google_advertise_image"];
    googleAdvertiseUrl = json["google_advertise_url"];
    googleAdvertiseStatus = json["google_advertise_status"];
    createdDate = json["created_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["google_advertise_id"] = googleAdvertiseId;
    map["google_advertise_name"] = googleAdvertiseName;
    map["google_advertise_image"] = googleAdvertiseImage;
    map["google_advertise_url"] = googleAdvertiseUrl;
    map["google_advertise_status"] = googleAdvertiseStatus;
    map["created_date"] = createdDate;
    return map;
  }
}
