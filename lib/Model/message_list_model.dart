class MessageList {
  bool status;
  dynamic statusCode;
  dynamic message;
  List<Data> data;

  MessageList({this.status, this.statusCode, this.message, this.data});

  MessageList.fromJson(dynamic json) {
    status = json["status"];
    statusCode = json["statusCode"];
    message = json["message"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data.add(Data.fromJson(v));
      });
    }
  }

  Map<dynamic, dynamic> toJson() {
    var map = <dynamic, dynamic>{};
    map["status"] = status;
    map["statusCode"] = statusCode;
    map["message"] = message;
    if (data != null) {
      map["data"] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  dynamic adminNotificationId;
  dynamic notiticationTitle;
  dynamic notificationMessage;
  dynamic isUser;
  dynamic isMediator;
  dynamic createdDate;
  dynamic msgUserAdminId;
  dynamic isUserStatus;

  Data({
    this.adminNotificationId,
    this.notiticationTitle,
    this.notificationMessage,
    this.isUser,
    this.isMediator,
    this.createdDate,
    this.msgUserAdminId,
    this.isUserStatus,
  });

  Data.fromJson(dynamic json) {
    adminNotificationId = json["admin_notification_id"];
    notiticationTitle = json["notitication_title"];
    notificationMessage = json["notification_message"];
    isUser = json["is_user"];
    isMediator = json["is_mediator"];
    createdDate = json["created_date"];
    msgUserAdminId = json["msg_user_admin_id"];
    isUserStatus = json["is_user_status"];
  }

  Map<dynamic, dynamic> toJson() {
    var map = <dynamic, dynamic>{};
    map["admin_notification_id"] = adminNotificationId;
    map["notitication_title"] = notiticationTitle;
    map["notification_message"] = notificationMessage;
    map["is_user"] = isUser;
    map["is_mediator"] = isMediator;
    map["created_date"] = createdDate;
    map["msg_user_admin_id"] = msgUserAdminId;
    map["is_user_status"] = isUserStatus;
    return map;
  }
}
