/// status : true
/// statusCode : 3
/// message : "Video list get successfully."
/// data : [{"video_id":1,"video_title":"Amazing Nature | Drone | Waterfalls | Aerial View ","video_thumb":"","video_files_path":"https://www.youtube.com/watch?v=F_7ZoAQ3aJM","video_file_upload_date":"2021-08-24 07:00:00"},{"video_id":2,"video_title":"beautiful & relaxing piano and flute songs to Relax, Study, Work or Sleep","video_thumb":"","video_files_path":"https://www.youtube.com/watch?v=pgRlZ2optXw","video_file_upload_date":"2021-08-24 03:00:00"},{"video_id":3,"video_title":"Disney RELAXING PIANO Collection -Sleep Music, Study Music, Calm Music","video_thumb":"","video_files_path":"https://www.youtube.com/watch?v=g8NVwN0_mks","video_file_upload_date":"2021-08-24 14:00:00"}]

class VideoList {
  bool status;
  int statusCode;
  String message;
  List<Data> data;

  VideoList({this.status, this.statusCode, this.message, this.data});

  VideoList.fromJson(dynamic json) {
    status = json["status"];
    statusCode = json["statusCode"];
    message = json["message"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["statusCode"] = statusCode;
    map["message"] = message;
    if (data != null) {
      map["data"] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// video_id : 1
/// video_title : "Amazing Nature | Drone | Waterfalls | Aerial View "
/// video_thumb : ""
/// video_files_path : "https://www.youtube.com/watch?v=F_7ZoAQ3aJM"
/// video_file_upload_date : "2021-08-24 07:00:00"

class Data {
  int videoId;
  String videoTitle;
  String videoThumb;
  String videoFilesPath;
  String videoFileUploadDate;
  String createdDate;

  Data(
      {this.videoId,
      this.videoTitle,
      this.videoThumb,
      this.videoFilesPath,
      this.videoFileUploadDate,
      this.createdDate});

  Data.fromJson(dynamic json) {
    videoId = json["video_id"];
    videoTitle = json["video_title"];
    videoThumb = json["video_thumb"];
    videoFilesPath = json["video_files_path"];
    videoFileUploadDate = json["video_file_upload_date"];
    createdDate = json["created_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["video_id"] = videoId;
    map["video_title"] = videoTitle;
    map["video_thumb"] = videoThumb;
    map["video_files_path"] = videoFilesPath;
    map["video_file_upload_date"] = videoFileUploadDate;
    map["video_file_upload_date"] = createdDate;

    return map;
  }
}
