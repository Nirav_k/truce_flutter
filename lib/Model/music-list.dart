/// status : true
/// statusCode : 3
/// message : "Music list get successfully."
/// data : [{"music_id":1,"music_title":"Relexing","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/1.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/1.wav","music_file_upload_date":"2021-08-31 00:00:00"},{"music_id":2,"music_title":"test ","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/2.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/2.wav","music_file_upload_date":"2021-08-31 00:00:00"},{"music_id":3,"music_title":"Relexing","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/3.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/3.wav","music_file_upload_date":"2021-08-31 00:00:00"},{"music_id":4,"music_title":"test ","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/4.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/4.wav","music_file_upload_date":"2021-08-31 00:00:00"},{"music_id":5,"music_title":"Relexing","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/5.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/5.wav","music_file_upload_date":"2021-08-31 00:00:00"},{"music_id":6,"music_title":"test ","music_thumb":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/6.PNG","music_file":"http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/6.wav","music_file_upload_date":"2021-08-31 00:00:00"}]

class MusicList {
  bool status;
  int statusCode;
  String message;
  List<Data> data;

  MusicList({this.status, this.statusCode, this.message, this.data});

  MusicList.fromJson(dynamic json) {
    status = json["status"];
    statusCode = json["statusCode"];
    message = json["message"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["statusCode"] = statusCode;
    map["message"] = message;
    if (data != null) {
      map["data"] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// music_id : 1
/// music_title : "Relexing"
/// music_thumb : "http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/1.PNG"
/// music_file : "http://192.168.1.15/Philly_Truce_app/public/storage/image/music_photo_and_files/1.wav"
/// music_file_upload_date : "2021-08-31 00:00:00"

class Data {
  int musicId;
  String musicTitle;
  String musicThumb;
  String musicFile;
  String musicFileUploadDate;
  String createdDate;

  Data(
      {this.musicId,
      this.musicTitle,
      this.musicThumb,
      this.musicFile,
      this.musicFileUploadDate,
      this.createdDate});

  Data.fromJson(dynamic json) {
    musicId = json["music_id"];
    musicTitle = json["music_title"];
    musicThumb = json["music_thumb"];
    musicFile = json["music_file"];
    musicFileUploadDate = json["music_file_upload_date"];
    createdDate = json["created_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["music_id"] = musicId;
    map["music_title"] = musicTitle;
    map["music_thumb"] = musicThumb;
    map["music_file"] = musicFile;
    map["music_file_upload_date"] = musicFileUploadDate;
    map["created_date"] = createdDate;
    return map;
  }
}
