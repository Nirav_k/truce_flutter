import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
// import 'package:youtube_player_iframe/youtube_player_iframe.dart' as iYou;
// import 'package:youtube_player_iframe/youtube_player_iframe.dart';

var locale = 'en';

// class YoutubeIFrame extends StatefulWidget {
//   const YoutubeIFrame({Key key}) : super(key: key);
//
//   @override
//   _YoutubeIFrameState createState() => _YoutubeIFrameState();
// }
//
// class _YoutubeIFrameState extends State<YoutubeIFrame> {
//   iYou.YoutubePlayerController _controller;
//
//   @override
//   void initState() {
//     super.initState();
//
//     _controller = iYou.YoutubePlayerController(
//       initialVideoId: 'tcodrIK2P_I',
//       params: iYou.YoutubePlayerParams(
//         playlist: [
//           'nPt8bK2gbaU',
//           'K18cpp_-gP8',
//           'iLnmTe5Q2Qw',
//           '_WoCV4c6XOE',
//           'KmzdUe0RSJo',
//           '6jZDSSZZxjQ',
//           'p2lYr3vM_1w',
//           '7QUtEmBT_-w',
//           '34_PXCzGw1M',
//         ],
//         startAt: Duration.zero,
//         showControls: true,
//         showFullscreenButton: true,
//         desktopMode: false,
//         privacyEnhanced: true,
//       ),
//     );
//     _controller.onEnterFullscreen = () {
//       SystemChrome.setPreferredOrientations([
//         DeviceOrientation.landscapeLeft,
//         DeviceOrientation.landscapeRight,
//       ]);
//       print('Entered Fullscreen');
//     };
//     _controller.onExitFullscreen = () {
//       print('Exited Fullscreen');
//     };
//   }
//
//   @override
//   void dispose() {
//     SystemChrome.setPreferredOrientations([
//       DeviceOrientation.portraitUp,
//     ]);
//     SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//       statusBarColor: Colors.transparent,
//     ));
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     const player = iYou.YoutubePlayerIFrame();
//     return iYou.YoutubePlayerControllerProvider(
//       controller: _controller,
//       child: Scaffold(
//         body: LayoutBuilder(
//           builder: (context, constraints) {
//             if (kIsWeb && constraints.maxWidth > 800) {
//               return Row(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Expanded(child: player),
//                 ],
//               );
//             }
//             return ListView(
//               children: [
//                 Stack(
//                   children: [
//                     player,
//                     Positioned.fill(
//                       child: iYou.YoutubeValueBuilder(
//                         controller: _controller,
//                         builder: (context, value) {
//                           return AnimatedCrossFade(
//                             firstChild: SizedBox.shrink(),
//                             secondChild: DecoratedBox(
//                               decoration: BoxDecoration(
//                                 image: DecorationImage(
//                                   image: NetworkImage(
//                                     iYou.YoutubePlayerController.getThumbnail(
//                                       videoId:
//                                           _controller.params.playlist.first,
//                                       quality: ThumbnailQuality.medium,
//                                     ),
//                                   ),
//                                   fit: BoxFit.fitWidth,
//                                 ),
//                               ),
//                               child: GestureDetector(
//                                   onTap: () {
//                                     Navigator.pop(context);
//                                   },
//                                   child: Center(child: Icon(Icons.close))),
//                             ),
//                             crossFadeState: value.isReady
//                                 ? CrossFadeState.showFirst
//                                 : CrossFadeState.showSecond,
//                             duration: const Duration(milliseconds: 300),
//                           );
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

class FlutterYoutubeViewDefault extends StatefulWidget {
  final String videoId;
  final List videoIdList;
  int index;

  FlutterYoutubeViewDefault({this.videoId, this.videoIdList, this.index});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<FlutterYoutubeViewDefault>
    implements YouTubePlayerListener {
  bool showPlayer = false;

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    super.initState();
  }

  double currentVideoSecond = 0.0;
  String playerState = "";
  FlutterYoutubeViewController controller;
  int isInitialise = -1;

  @override
  void onCurrentSecond(double second) {
    print("onCurrentSecond second = $second");
    currentVideoSecond = second;
  }

  @override
  void onError(String error) {
    print("onError error = $error");
  }

  @override
  void onReady() {
    print("onReady");
  }

  @override
  void onStateChange(String state) {
    print("onStateChange state = $state");
    setState(() {
      playerState = state;
    });

    if (state == "UNSTARTED") {
      setState(() {
        showPlayer = true;
      });
    }
    if (state == "ENDED") {
      setState(() {
        widget.index = widget.index + 1;
      });
      onReady();
      _onYoutubeCreated(controller);
    }
  }

  @override
  void onVideoDuration(double duration) {
    print("onVideoDuration duration = $duration");
  }

  void _onYoutubeCreated(FlutterYoutubeViewController controller) {
    this.controller = controller;
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          fit: StackFit.expand,
          children: [
            FlutterYoutubeView(
              onViewCreated: _onYoutubeCreated,
              listener: this,
              scaleMode: YoutubeScaleMode.fitHeight,
              params: YoutubeParam(
                  videoId: widget.videoIdList[widget.index],
                  showUI: true,
                  startSeconds: 0.0,
                  showYoutube: false,
                  autoPlay: true),
            ),
            Positioned(
              left: 5,
              top: 5,
              child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios_outlined,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            )
          ],
        ));
  }
}

// class FlutterYoutubeViewCustomWidget extends StatefulWidget {
//   @override
//   _MyAppCustomState createState() => _MyAppCustomState();
// }
//
// class _MyAppCustomState extends State<FlutterYoutubeViewCustomWidget>
//     implements YouTubePlayerListener {
//   double _volume = 50;
//   double _videoDuration = 0.0;
//   double _currentVideoSecond = 0.0;
//   String _playerState = "";
//   FlutterYoutubeViewController _controller;
//   YoutubeScaleMode _mode = YoutubeScaleMode.none;
//
//   // PlaybackRate _playbackRate = PlaybackRate.RATE_1;
//   bool _isMuted = false;
//
//   @override
//   void onCurrentSecond(double second) {
//     // print("onCurrentSecond second = $second");
//     _currentVideoSecond = second;
//   }
//
//   @override
//   void onError(String error) {
//     print("onError error = $error");
//   }
//
//   @override
//   void onReady() {
//     print("onReady");
//   }
//
//   @override
//   void onStateChange(String state) {
//     print("onStateChange state = $state");
//     setState(() {
//       _playerState = state;
//     });
//   }
//
//   @override
//   void onVideoDuration(double duration) {
//     print("onVideoDuration duration = $duration");
//   }
//
//   void _onYoutubeCreated(FlutterYoutubeViewController controller) {
//     this._controller = controller;
//   }
//
//   void _loadOrCueVideo() {
//     _controller.loadOrCueVideo('gcj2RUWQZ60', _currentVideoSecond);
//   }
//
//   void _play() {
//     _controller.play();
//   }
//
//   void _pause() {
//     _controller.pause();
//   }
//
//   void _seekTo(double time) {
//     _controller.seekTo(time);
//   }
//
//   void _setVolume(int volumePercent) {
//     _controller.setVolume(volumePercent);
//   }
//
//   void _changeScaleMode(YoutubeScaleMode mode) {
//     setState(() {
//       _mode = mode;
//       _controller.changeScaleMode(mode);
//     });
//   }
//
//   void _changeVolumeMode(bool isMuted) {
//     setState(() {
//       _isMuted = isMuted;
//       if (isMuted) {
//         _controller.setMute();
//       } else {
//         _controller.setUnMute();
//       }
//     });
//   }
//
//   // void _changePlaybackRate(PlaybackRate playbackRate) {
//   //   setState(() {
//   //     _playbackRate = playbackRate;
//   //     _controller.setPlaybackRate(rate: playbackRate);
//   //   });
//   // }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(title: const Text('Custom UI')),
//         body: Stack(
//           children: <Widget>[
//             Container(
//                 child: FlutterYoutubeView(
//               scaleMode: _mode,
//               onViewCreated: _onYoutubeCreated,
//               listener: this,
//               params: YoutubeParam(
//                 videoId: 'gcj2RUWQZ60',
//                 showUI: false,
//                 startSeconds: 0.0,
//                 autoPlay: true,
//               ),
//             )),
//             Column(
//               children: <Widget>[
//                 Text(
//                   'Current state: $_playerState',
//                   style: TextStyle(color: Colors.blue),
//                 ),
//                 RaisedButton(
//                   onPressed: _loadOrCueVideo,
//                   child: Text('Click reload video'),
//                 ),
//                 _buildControl(),
//                 _buildVolume(),
//                 _buildScaleModeRadioGroup(),
//                 // _buildPlaybackRate()
//               ],
//             )
//           ],
//         ));
//   }
//
//   Widget _buildControl() {
//     return new Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: <Widget>[
//         RaisedButton(
//           onPressed: _play,
//           child: Text('Play'),
//         ),
//         RaisedButton(
//           onPressed: _pause,
//           child: Text('Pause'),
//         ),
//         RaisedButton(
//           onPressed: () {
//             _seekTo(20.0);
//           },
//           child: Text('seekTo 20s'),
//         )
//       ],
//     );
//   }
//
//   Widget _buildScaleModeRadioGroup() {
//     return new Row(
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         new Radio(
//           value: YoutubeScaleMode.none,
//           groupValue: _mode,
//           onChanged: _changeScaleMode,
//         ),
//         new Text(
//           'none',
//           style: TextStyle(color: Colors.blue),
//         ),
//         new Radio(
//           value: YoutubeScaleMode.fitWidth,
//           groupValue: _mode,
//           onChanged: _changeScaleMode,
//         ),
//         new Text(
//           'fitWidth',
//           style: TextStyle(color: Colors.blue),
//         ),
//         new Radio(
//           value: YoutubeScaleMode.fitHeight,
//           groupValue: _mode,
//           onChanged: _changeScaleMode,
//         ),
//         new Text(
//           'fitHeight',
//           style: TextStyle(color: Colors.blue),
//         ),
//       ],
//     );
//   }
//
//   Widget _buildVolume() {
//     return new Row(
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         new Radio(
//           value: false,
//           groupValue: _isMuted,
//           onChanged: _changeVolumeMode,
//         ),
//         new Text(
//           'unMute',
//           style: TextStyle(color: Colors.blue),
//         ),
//         new Radio(
//           value: true,
//           groupValue: _isMuted,
//           onChanged: _changeVolumeMode,
//         ),
//         new Text(
//           'Mute',
//           style: TextStyle(color: Colors.blue),
//         )
//       ],
//     );
//   }
//
// // Widget _buildPlaybackRate() {
// //   return new Row(
// //     mainAxisAlignment: MainAxisAlignment.center,
// //     children: <Widget>[
// //       new Radio(
// //         value: PlaybackRate.RATE_0_25,
// //         groupValue: _playbackRate,
// //         onChanged: _changePlaybackRate,
// //       ),
// //       new Text(
// //         '0_25',
// //         style: TextStyle(color: Colors.blue),
// //       ),
// //       new Radio(
// //         value: PlaybackRate.RATE_0_5,
// //         groupValue: _playbackRate,
// //         onChanged: _changePlaybackRate,
// //       ),
// //       new Text(
// //         '0_5',
// //         style: TextStyle(color: Colors.blue),
// //       ),
// //       new Radio(
// //         value: PlaybackRate.RATE_1,
// //         groupValue: _playbackRate,
// //         onChanged: _changePlaybackRate,
// //       ),
// //       new Text(
// //         '1',
// //         style: TextStyle(color: Colors.blue),
// //       ),
// //       new Radio(
// //         value: PlaybackRate.RATE_1_5,
// //         groupValue: _playbackRate,
// //         onChanged: _changePlaybackRate,
// //       ),
// //       new Text(
// //         '1_5',
// //         style: TextStyle(color: Colors.blue),
// //       ),
// //       new Radio(
// //         value: PlaybackRate.RATE_2,
// //         groupValue: _playbackRate,
// //         onChanged: _changePlaybackRate,
// //       ),
// //       new Text(
// //         '2',
// //         style: TextStyle(color: Colors.blue),
// //       )
// //     ],
// //   );
// // }
// }
