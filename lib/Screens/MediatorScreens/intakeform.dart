import 'package:flutter/material.dart';
import 'package:flutter_app/Model/user_inforamation_model.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class INTAKEFormScreen extends StatefulWidget {
  final Data peerData;

  const INTAKEFormScreen({Key key, this.peerData}) : super(key: key);

  @override
  _INTAKEFormScreenState createState() => _INTAKEFormScreenState();
}

class _INTAKEFormScreenState extends State<INTAKEFormScreen> {
  final flutterWebviewPlugin = FlutterWebviewPlugin();

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: WebviewScaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Image.asset('assets/images/back_ic.png'),
          ),
          shadowColor: Colors.transparent,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: TextWidgets().boldTextWidget(
              homeButtonTextColor,
              Strings.appName,
              context,
              ResponsiveFlutter.of(context).fontSize(3.0)),
        ),
        url: Uri.encodeFull(
            "https://docs.google.com/forms/d/e/1FAIpQLSdz0qRv5D43oeKc11lCL88EFcqwhDKJAfCeR821163Iq_hJfg/viewform?&entry.455389909=${widget.peerData.help_id}&entry.2049979187=${widget.peerData.name}&entry.1041452121=${widget.peerData.last_name}&entry.1427906808=${widget.peerData.email}"),
        clearCookies: false,
        clearCache: false,
        hidden: true,
        appCacheEnabled: false,
        supportMultipleWindows: true,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    flutterWebviewPlugin.dispose();
    super.dispose();
  }
}
