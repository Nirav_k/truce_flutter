import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Main/Bloc/videolist-bloc.dart';
import 'package:flutter_app/Model/music-list.dart';
import 'package:flutter_app/Screens/cutomise_ad_screen.dart';
import 'package:flutter_app/Utils/SizeConfig.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Widget/backgorund.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:just_audio/just_audio.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

// class MusicScreen extends StatefulWidget {
//   const MusicScreen({Key key}) : super(key: key);
//
//   @override
//   _MusicScreenState createState() => _MusicScreenState();
// }
//
// class _MusicScreenState extends State<MusicScreen> {
//   int selectedIndex = -1;
//
//   Duration _duration = new Duration();
//   Duration _position = new Duration(seconds: 0);
//
//   AudioPlayer audioPlayer;
//   double _value = 0.00;
//   bool isPicked = false;
//
//   AudioPlayerState state;
//
//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         leading: InkResponse(
//           onTap: () => Navigator.pop(context),
//           child: Image.asset('assets/images/back_ic.png'),
//         ),
//         shadowColor: Colors.transparent,
//         elevation: 0.0,
//         backgroundColor: Colors.transparent,
//         centerTitle: true,
//         title: TextWidgets().boldTextWidget(homeButtonTextColor, 'Music',
//             context, ResponsiveFlutter.of(context).fontSize(3.0)),
//       ),
//       body: Container(
//         width: SizeConfig.safeBlockHorizontal * 100,
//         height: SizeConfig.safeBlockVertical * 100,
//         decoration: backgroundBoxDecoration,
//         child: Card(
//           margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
//           elevation: 1.0,
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.elliptical(
//                   ResponsiveFlutter.of(context).hp(2.0),
//                   ResponsiveFlutter.of(context).hp(2.0)))),
//           child: StreamBuilder(
//             stream: getVideoListBloc.subMusic.stream,
//             builder: (BuildContext context, AsyncSnapshot<MusicList> snapshot) {
//               if (snapshot.hasData) {
//                 // if (snapshot.data.data.length != mapDuration.keys.length) {
//                 // getDuratuion(snapshot, 0);
//                 // }
//                 return ListView.builder(
//                   itemCount: snapshot.data.data.length,
//                   itemBuilder: (BuildContext context, int index) {
//                     return Container(
//                       margin: EdgeInsets.symmetric(
//                         vertical: ResponsiveFlutter.of(context).scale(5.0),
//                         horizontal: ResponsiveFlutter.of(context).scale(10.0),
//                       ),
//                       padding:
//                           EdgeInsets.all(ResponsiveFlutter.of(context).hp(1.5)),
//                       decoration: BoxDecoration(
//                         color: videoBackColor,
//                         borderRadius: BorderRadius.all(
//                           Radius.elliptical(
//                             ResponsiveFlutter.of(context).hp(2.0),
//                             ResponsiveFlutter.of(context).hp(2.0),
//                           ),
//                         ),
//                       ),
//                       height: ResponsiveFlutter.of(context).hp(16),
//                       alignment: Alignment.topCenter,
//                       child: Row(
//                         children: [
//                           Flexible(
//                             flex: 4,
//                             child: Container(
//                               height: double.infinity,
//                               decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.all(
//                                   Radius.circular(
//                                     ResponsiveFlutter.of(context).scale(8.0),
//                                   ),
//                                 ),
//                               ),
//                               child: GestureDetector(
//                                 onTap: () async {
//                                   if (audioPlayer == null) {
//                                     playAudio(
//                                         snapshot.data.data[index].musicFile,
//                                         index);
//                                     return;
//                                   }
//                                   if (selectedIndex == index) {
//                                     // if (state == null)
//                                     //   playAudio(
//                                     //       snapshot.data.data[index].musicFile);
//                                     // else
//                                     if (state == AudioPlayerState.PAUSED)
//                                       await resumeAudio();
//                                     // else if (state ==
//                                     //     AudioPlayerState.COMPLETED)
//                                     //   playAudio(
//                                     //       snapshot.data.data[index].musicFile);
//                                     else
//                                       await pauseAudio();
//                                   } else
//                                     playAudio(
//                                         snapshot.data.data[index].musicFile,
//                                         index);
//                                   selectedIndex = index;
//                                   setState(() {});
//                                 },
//                                 child: Stack(
//                                   children: [
//                                     Container(
//                                       clipBehavior: Clip.hardEdge,
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.all(
//                                           Radius.circular(
//                                             ResponsiveFlutter.of(context)
//                                                 .scale(8.0),
//                                           ),
//                                         ),
//                                       ),
//                                       child: Image(
//                                         image: NetworkImage(
//                                           snapshot.data.data[index].musicThumb,
//                                         ),
//                                         fit: BoxFit.cover,
//                                         width: double.infinity,
//                                       ),
//                                     ),
//                                     Center(
//                                       child: Image.asset(
//                                         selectedIndex == index
//                                             ? state == AudioPlayerState.PLAYING
//                                                 ? 'assets/images/pause_ic.png'
//                                                 : 'assets/images/play_ic.png'
//                                             : 'assets/images/play_ic.png',
//                                       ),
//                                     ),
//                                     Align(
//                                       alignment: Alignment.bottomRight,
//                                       child: Container(
//                                         margin: EdgeInsets.all(
//                                           ResponsiveFlutter.of(context)
//                                               .scale(5.0),
//                                         ),
//                                         alignment: Alignment.center,
//                                         width: 30,
//                                         height: 15,
//                                         decoration: BoxDecoration(
//                                           color: Colors.white,
//                                           borderRadius: BorderRadius.all(
//                                             Radius.circular(
//                                               ResponsiveFlutter.of(context)
//                                                   .scale(10.0),
//                                             ),
//                                           ),
//                                         ),
//                                         child: Text(
//                                           mapDuration[snapshot.data.data[index]
//                                                       .musicFile] !=
//                                                   null
//                                               ? '${mapDuration[snapshot.data.data[index].musicFile]}'
//                                               : "--",
//                                           textAlign: TextAlign.center,
//                                           overflow: TextOverflow.ellipsis,
//                                           maxLines: 1,
//                                           style: TextStyle(
//                                             fontSize:
//                                                 ResponsiveFlutter.of(context)
//                                                     .fontSize(1.3),
//                                             fontFamily: fontStyle,
//                                             color: Colors.black,
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ),
//                           Flexible(
//                             flex: 7,
//                             child: Container(
//                               padding: EdgeInsets.only(
//                                   left: ResponsiveFlutter.of(context).hp(2.0)),
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 mainAxisAlignment: MainAxisAlignment.start,
//                                 children: [
//                                   Container(
//                                     child: Text(
//                                       snapshot.data.data[index].musicTitle,
//                                       textAlign: TextAlign.start,
//                                       overflow: TextOverflow.ellipsis,
//                                       maxLines: 1,
//                                       style: TextStyle(
//                                         fontSize: ResponsiveFlutter.of(context)
//                                             .fontSize(1.8),
//                                         fontFamily: fontStyle,
//                                         color: Colors.black,
//                                       ),
//                                     ),
//                                   ),
//                                   Container(
//                                     child: IgnorePointer(
//                                       ignoring: selectedIndex != index,
//                                       child: Slider(
//                                         min: 0.0,
//                                         max: _duration.inSeconds.toDouble(),
//                                         activeColor: homeButtonTextColor,
//                                         inactiveColor: Colors.white,
//                                         value: selectedIndex == index
//                                             ? isPicked
//                                                 ? _value
//                                                 : _position.inSeconds.toDouble()
//                                             : 0,
//                                         onChangeEnd: (value) {
//                                           isPicked = false;
//                                           seekAudio(value.toInt());
//                                         },
//                                         onChanged: (value) {
//                                           if (selectedIndex == index) {
//                                             isPicked = true;
//                                             _value = value;
//                                             setState(() {});
//                                             seekAudio(value.toInt());
//                                           }
//                                         },
//                                       ),
//                                     ),
//                                   ),
//                                   Container(
//                                     alignment: Alignment.bottomRight,
//                                     child: Text(
//                                       StringExtension.displayTimeAgoFromTimestamp(
//                                           "${snapshot.data.data[index].createdDate}"),
//                                       textAlign: TextAlign.end,
//                                       overflow: TextOverflow.ellipsis,
//                                       maxLines: 1,
//                                       style: TextStyle(
//                                         fontSize: ResponsiveFlutter.of(context)
//                                             .fontSize(1.5),
//                                         fontFamily: fontStyle,
//                                         color: Colors.black,
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     );
//                   },
//                 );
//               } else if (snapshot.hasError)
//                 return Center(
//                   child: Container(
//                     child: Text('Music not available'),
//                   ),
//                 );
//               else
//                 return Center(
//                   child: Container(
//                     child: CircularProgressIndicator(),
//                   ),
//                 );
//             },
//           ),
//         ),
//       ),
//     );
//   }
//
//   @override
//   void dispose() {
//     audioPlayer.release();
//     audioPlayer.dispose();
//     mapDuration.clear();
//     super.dispose();
//   }
//
//   Map<String, dynamic> mapDuration = Map();
//   AudioPlayer ap = AudioPlayer();
//
//   // getDuratuion(AsyncSnapshot<MusicList> snapshot, int pos) {
//   //   ap.setUrl(snapshot.data.data[pos].musicFile).then((value) async {
//   //     print(value);
//   //     ap.getDuration().then((dur) async {
//   //       Duration sec = Duration(milliseconds: dur);
//   //       print(">> ${sec.abs()}");
//   //       var ss = "$sec";
//   //       var cc = ss.split(":");
//   //       print("${cc[0]} ${cc[1]} ${cc[2].split(".")[0]}");
//   //       var HMS;
//   //
//   //       if (int.parse(cc[0]) > 0) {
//   //         HMS = "${cc[0]}:${cc[1]}:${cc[2].split(".")[0]}";
//   //       } else {
//   //         HMS = "${cc[1]}:${cc[2].split(".")[0]}";
//   //       }
//   //
//   //       // if (sec.inHours > 0) {
//   //       //   HMS = "${sec.inHours}:${sec.inMinutes}:${sec.inSeconds}";
//   //       // } else {
//   //       //   HMS = "${sec.inMinutes}:${sec.inSeconds}";
//   //       // }
//   //       mapDuration[snapshot.data.data[pos].musicFile] = HMS;
//   //       // await ap.dispose();
//   //       // await ap.release();
//   //       if (pos != snapshot.data.data.length - 1) {
//   //         getDuratuion(snapshot, pos + 1);
//   //       } else {
//   //         print(mapDuration);
//   //         ap.stop();
//   //         ap.release();
//   //         ap.dispose();
//   //         setState(() {});
//   //       }
//   //     }).catchError((onError) {
//   //       print(" ONEROxfghxghOR ?? >>  ${onError.toString()}");
//   //     });
//   //   }).catchError((onError) {
//   //     print(" ONEROOR ?? >>  ${onError.toString()}");
//   //   });
//   // }
//
//   playAudio(url, index) async {
//     setState(() {
//       selectedIndex = index;
//     });
//     if (audioPlayer == null) {
//       initPlayer();
//     }
//     _position = Duration(seconds: 0);
//     setState(() {});
//     if (state == AudioPlayerState.PLAYING) {
//       await pauseAudio();
//       await audioPlayer.play(url).then((value) {
//         print("Play");
//         state = AudioPlayerState.PLAYING;
//         setState(() {});
//       });
//     } else {
//       await audioPlayer.play(url).then((value) {
//         print("Play");
//         state = AudioPlayerState.PLAYING;
//         setState(() {});
//       });
//     }
//   }
//
//   seekAudio(int durationInSecond) async {
//     await audioPlayer.seek(Duration(seconds: durationInSecond));
//     setState(() {});
//   }
//
//   pauseAudio() async {
//     await audioPlayer.pause().then((value) {
//       print("Pause");
//       state = AudioPlayerState.PAUSED;
//       setState(() {});
//     });
//   }
//
//   stopAudio() async {
//     await audioPlayer.stop().then((value) {
//       print("Stop");
//       state = AudioPlayerState.STOPPED;
//       setState(() {});
//     });
//   }
//
//   resumeAudio() async {
//     await audioPlayer.resume().then((value) {
//       print("Resume");
//       state = AudioPlayerState.PLAYING;
//       setState(() {});
//     });
//   }
//
//   @override
//   void initState() {
//     apiCall();
//     super.initState();
//     initPlayer();
//   }
//
//   apiCall() async {
//     await getVideoListBloc.getMusicList();
//   }
//
//   initPlayer() {
//     audioPlayer = AudioPlayer();
//
//     audioPlayer.onDurationChanged.listen((event) {
//       print("Duration >> ${event.inSeconds}");
//       _duration = event;
//     });
//
//     audioPlayer.onAudioPositionChanged.listen((event) {
//       // print("Positioned >> ${event.inSeconds}");
//       if (!isPicked) {
//         _position = Duration(seconds: event.inSeconds);
//         setState(() {});
//       }
//     });
//     audioPlayer.onPlayerCompletion.listen((event) {
//       print("<<< PlayerCompletion >>>");
//     });
//     audioPlayer.onPlayerStateChanged.listen((event) {
//       print("<<< State Changed >>> $event");
//       if (event == AudioPlayerState.COMPLETED) {
//         _position = Duration(seconds: 0);
//         state = AudioPlayerState.COMPLETED;
//
//         audioPlayer.pause();
//         audioPlayer.dispose();
//         audioPlayer = null;
//         selectedIndex = -1;
//
//         setState(() {});
//       }
//     });
//   }
// }

class MusicScreen extends StatefulWidget {
  const MusicScreen({Key key}) : super(key: key);

  @override
  _MusicScreenState createState() => _MusicScreenState();
}

class _MusicScreenState extends State<MusicScreen> {
  int selectedIndex = -1;
  int playIndex = 0;

  Duration _duration = new Duration();
  Duration _position = new Duration(seconds: 0);

  AudioPlayer audioPlayer;
  double _value = 0.00;
  bool isPicked = false;
  int audioInit = -1;

  // @override
  // Widget build(BuildContext context) {
  //   SizeConfig().init(context);
  //
  //   return Scaffold(
  //     resizeToAvoidBottomInset: false,
  //     backgroundColor: Colors.white,
  //     appBar: AppBar(
  //       leading: InkResponse(
  //         onTap: () => Navigator.pop(context),
  //         child: Image.asset('assets/images/back_ic.png'),
  //       ),
  //       shadowColor: Colors.transparent,
  //       elevation: 0.0,
  //       backgroundColor: Colors.transparent,
  //       centerTitle: true,
  //       title: TextWidgets().boldTextWidget(homeButtonTextColor, 'Music',
  //           context, ResponsiveFlutter.of(context).fontSize(3.0)),
  //     ),
  //     body: Container(
  //       width: SizeConfig.safeBlockHorizontal * 100,
  //       height: SizeConfig.safeBlockVertical * 100,
  //       decoration: backgroundBoxDecoration,
  //       child: Card(
  //         margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
  //         elevation: 1.0,
  //         child: Column(
  //           children: [
  //             ValueListenableBuilder<ProgressBarState>(
  //               valueListenable: progressNotifier,
  //               builder: (_, value, __) {
  //                 return ProgressBar(
  //                   progress: value.current,
  //                   buffered: value.buffered,
  //                   total: value.total,
  //                   onSeek: seek,
  //                 );
  //               },
  //             ),
  //             ValueListenableBuilder<ButtonState>(
  //               valueListenable: buttonNotifier,
  //               builder: (_, value, __) {
  //                 switch (value) {
  //                   case ButtonState.loading:
  //                     return Container(
  //                       margin: EdgeInsets.all(8.0),
  //                       width: 32.0,
  //                       height: 32.0,
  //                       child: CircularProgressIndicator(),
  //                     );
  //                     break;
  //                   case ButtonState.paused:
  //                     return IconButton(
  //                       icon: Icon(Icons.play_arrow),
  //                       iconSize: 32.0,
  //                       onPressed: () {
  //                         if (audioInit == -1) {
  //                           init();
  //                         }
  //                         play();
  //                       },
  //                     );
  //                     break;
  //                   case ButtonState.playing:
  //                     return IconButton(
  //                       icon: Icon(Icons.pause),
  //                       iconSize: 32.0,
  //                       onPressed: pause,
  //                     );
  //                     break;
  //                 }
  //               },
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkResponse(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/back_ic.png'),
        ),
        shadowColor: Colors.transparent,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TextWidgets().boldTextWidget(homeButtonTextColor, 'Music',
            context, ResponsiveFlutter.of(context).fontSize(3.0)),
      ),
      body: Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 100,
        decoration: backgroundBoxDecoration,
        child: Column(
          children: [
            Expanded(flex: 0, child: CustomiseAdListScreen()),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
                elevation: 1.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(
                        ResponsiveFlutter.of(context).hp(2.0),
                        ResponsiveFlutter.of(context).hp(2.0)))),
                child: StreamBuilder(
                  stream: getVideoListBloc.subMusic.stream,
                  builder: (BuildContext context,
                      AsyncSnapshot<MusicList> snapshot) {
                    if (snapshot.hasData) {
                      snapshot.data.data.sort((a, b) =>
                          DateTime.parse(b.createdDate).toLocal().compareTo(
                              DateTime.parse(a.createdDate).toLocal()));
                      return snapshot.data.data != null
                          ? ListView.builder(
                              itemCount: snapshot.data.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                if (snapshot.data.data.length !=
                                    mapDuration.keys.length) {
                                  getDuration(snapshot, 0);
                                }
                                return Container(
                                  margin: EdgeInsets.symmetric(
                                    vertical: ResponsiveFlutter.of(context)
                                        .scale(5.0),
                                    horizontal: ResponsiveFlutter.of(context)
                                        .scale(10.0),
                                  ),
                                  padding: EdgeInsets.all(
                                      ResponsiveFlutter.of(context).hp(1.5)),
                                  decoration: BoxDecoration(
                                    color: videoBackColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.elliptical(
                                        ResponsiveFlutter.of(context).hp(2.0),
                                        ResponsiveFlutter.of(context).hp(2.0),
                                      ),
                                    ),
                                  ),
                                  height: ResponsiveFlutter.of(context).hp(16),
                                  alignment: Alignment.topCenter,
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 4,
                                        child: Container(
                                          height: double.infinity,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                ResponsiveFlutter.of(context)
                                                    .scale(8.0),
                                              ),
                                            ),
                                          ),
                                          child: GestureDetector(
                                            onTap: () async {},
                                            child: Stack(
                                              fit: StackFit.expand,
                                              children: [
                                                Container(
                                                  clipBehavior: Clip.hardEdge,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .scale(8.0),
                                                      ),
                                                    ),
                                                  ),
                                                  // child: Image(
                                                  //   image: NetworkImage(
                                                  //     snapshot.data.data[index].musicThumb,
                                                  //   ),
                                                  //   fit: BoxFit.cover,
                                                  //   width: double.infinity,
                                                  // ),
                                                  child: CachedNetworkImage(
                                                    fit: BoxFit.cover,
                                                    imageUrl:
                                                        "${snapshot.data.data[index].musicThumb}",
                                                    placeholder:
                                                        (context, url) =>
                                                            Container(
                                                      color: Colors.grey,
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Container(
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                                Center(
                                                  child: ValueListenableBuilder<
                                                      ButtonState>(
                                                    valueListenable:
                                                        buttonNotifier,
                                                    builder: (_, value, __) {
                                                      switch (value) {
                                                        case ButtonState
                                                            .loading:
                                                          return Container(
                                                            margin:
                                                                EdgeInsets.all(
                                                                    8.0),
                                                            width: 32.0,
                                                            height: 32.0,
                                                            child: selectedIndex ==
                                                                    index
                                                                ? CircularProgressIndicator(
                                                                    valueColor: new AlwaysStoppedAnimation<
                                                                            Color>(
                                                                        Colors
                                                                            .white))
                                                                : Image.asset(
                                                                    'assets/images/play_ic.png'),
                                                          );
                                                          break;
                                                        case ButtonState.paused:
                                                          // return IconButton(
                                                          //   icon: selectedIndex == index
                                                          //       ? value ==
                                                          //               ButtonState.paused
                                                          //           ? Icon(Icons.play_arrow)
                                                          //           : Icon(Icons.pause)
                                                          //       : Icon(Icons.play_arrow),
                                                          //   iconSize: 32.0,
                                                          //   onPressed: () {
                                                          //     selectedIndex = index;
                                                          //     setState(() {});
                                                          //     if (selectedIndex == index) {
                                                          //       if (audioInit == -1) {
                                                          //         init(snapshot
                                                          //             .data
                                                          //             .data[index]
                                                          //             .musicFile);
                                                          //       }
                                                          //       play();
                                                          //     } else {
                                                          //       if (value ==
                                                          //           ButtonState.playing) {
                                                          //         stop();
                                                          //         init(snapshot
                                                          //             .data
                                                          //             .data[index]
                                                          //             .musicFile);
                                                          //         play();
                                                          //       }
                                                          //     }
                                                          //   },
                                                          // );
                                                          return GestureDetector(
                                                            onTap: () {
                                                              if (selectedIndex ==
                                                                  index) {
                                                                if (audioInit ==
                                                                    -1) {
                                                                  setState(() {
                                                                    playIndex =
                                                                        index;
                                                                  });
                                                                  init(snapshot,
                                                                      index);
                                                                }
                                                                play();
                                                              } else {
                                                                pause();
                                                                setState(() {
                                                                  playIndex =
                                                                      index;
                                                                });
                                                                init(snapshot,
                                                                    index);
                                                                play();
                                                              }
                                                              selectedIndex =
                                                                  index;
                                                              setState(() {});
                                                            },
                                                            child: Image.asset(
                                                              selectedIndex ==
                                                                      index
                                                                  ? value ==
                                                                          ButtonState
                                                                              .paused
                                                                      ? 'assets/images/play_ic.png'
                                                                      : 'assets/images/pause_ic.png'
                                                                  : 'assets/images/play_ic.png',
                                                            ),
                                                          );
                                                          break;
                                                        case ButtonState
                                                            .playing:
                                                          // return IconButton(
                                                          //   icon: selectedIndex == index
                                                          //       ? value ==
                                                          //               ButtonState.playing
                                                          //           ? Icon(Icons.pause)
                                                          //           : Icon(Icons.play_arrow)
                                                          //       : Icon(Icons.play_arrow),
                                                          //   iconSize: 32.0,
                                                          //   onPressed: () {
                                                          //     if (selectedIndex == index) {
                                                          //       pause();
                                                          //     } else {
                                                          //       pause();
                                                          //       init(snapshot.data
                                                          //           .data[index].musicFile);
                                                          //       selectedIndex = index;
                                                          //       setState(() {});
                                                          //       play();
                                                          //     }
                                                          //   },
                                                          // );
                                                          return GestureDetector(
                                                            onTap: () {
                                                              if (selectedIndex ==
                                                                  index) {
                                                                pause();
                                                              } else {
                                                                pause();
                                                                setState(() {
                                                                  playIndex =
                                                                      index;
                                                                });
                                                                init(snapshot,
                                                                    index);
                                                                selectedIndex =
                                                                    index;
                                                                setState(() {});
                                                                play();
                                                              }
                                                            },
                                                            child: Image.asset(
                                                              selectedIndex ==
                                                                      index
                                                                  ? value ==
                                                                          ButtonState
                                                                              .playing
                                                                      ? 'assets/images/pause_ic.png'
                                                                      : 'assets/images/play_ic.png'
                                                                  : 'assets/images/play_ic.png',
                                                            ),
                                                          );
                                                          break;
                                                      }
                                                      return Container();
                                                    },
                                                  ),
                                                ),
                                                // Center(
                                                //   child: Image.asset(
                                                //     selectedIndex == index
                                                //         ? state == AudioPlayerState.PLAYING
                                                //         ? 'assets/images/pause_ic.png'
                                                //         : 'assets/images/play_ic.png'
                                                //         : 'assets/images/play_ic.png',
                                                //   ),
                                                // ),
                                                Align(
                                                  alignment:
                                                      Alignment.bottomRight,
                                                  child: Container(
                                                    margin: EdgeInsets.all(
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .scale(5.0),
                                                    ),
                                                    alignment: Alignment.center,
                                                    width: 30,
                                                    height: 15,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(
                                                          ResponsiveFlutter.of(
                                                                  context)
                                                              .scale(10.0),
                                                        ),
                                                      ),
                                                    ),
                                                    child: Text(
                                                      mapDuration[snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .musicFile] !=
                                                              null
                                                          ? '${mapDuration[snapshot.data.data[index].musicFile]}'
                                                          : "--",
                                                      textAlign:
                                                          TextAlign.center,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize:
                                                            ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(1.3),
                                                        fontFamily: fontStyle,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        flex: 7,
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              left:
                                                  ResponsiveFlutter.of(context)
                                                      .hp(2.0)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                child: Text(
                                                  snapshot.data.data[index]
                                                      .musicTitle,
                                                  textAlign: TextAlign.start,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                    fontSize:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .fontSize(1.8),
                                                    fontFamily: fontStyle,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                              IgnorePointer(
                                                ignoring:
                                                    selectedIndex != index,
                                                child: ValueListenableBuilder<
                                                    ProgressBarState>(
                                                  valueListenable:
                                                      progressNotifier,
                                                  builder: (_, value, __) {
                                                    return ProgressBar(
                                                      baseBarColor:
                                                          Colors.white,
                                                      bufferedBarColor:
                                                          Colors.grey.shade300,
                                                      progressBarColor:
                                                          homeButtonTextColor,
                                                      thumbColor:
                                                          homeButtonTextColor,
                                                      timeLabelLocation:
                                                          TimeLabelLocation
                                                              .none,
                                                      progress:
                                                          selectedIndex == index
                                                              ? value.current
                                                              : Duration.zero,
                                                      buffered:
                                                          selectedIndex == index
                                                              ? value.buffered
                                                              : Duration.zero,
                                                      total:
                                                          selectedIndex == index
                                                              ? value.total
                                                              : Duration.zero,
                                                      onSeek: seek,
                                                    );
                                                  },
                                                ),
                                              ),
                                              Container(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: Text(
                                                  StringExtension
                                                      .displayTimeAgoFromTimestamp(
                                                          "${snapshot.data.data[index].createdDate}"),
                                                  textAlign: TextAlign.end,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                    fontSize:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .fontSize(1.5),
                                                    fontFamily: fontStyle,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            )
                          : Center(
                              child: Container(
                                child: Text('Music not available'),
                              ),
                            );
                    } else if (snapshot.hasError)
                      return Center(
                        child: Container(
                          child: Text('Music not available'),
                        ),
                      );
                    else
                      return Center(
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    _audioPlayer = AudioPlayer();
    apDuration = AudioPlayer();
    apiCall();
    super.initState();
  }

  apiCall() async {
    await getVideoListBloc.getMusicList();
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    apDuration.dispose();
    super.dispose();
  }

  final progressNotifier = ValueNotifier<ProgressBarState>(
    ProgressBarState(
      current: Duration.zero,
      buffered: Duration.zero,
      total: Duration.zero,
    ),
  );
  final buttonNotifier = ValueNotifier<ButtonState>(ButtonState.paused);

  AudioPlayer _audioPlayer;
  AudioPlayer apDuration;

  Future<dynamic> init(AsyncSnapshot<MusicList> snapshot, int index) async {
    _audioPlayer.dispose();
    _audioPlayer = AudioPlayer();

    // initialize the song
    setState(() {
      selectedIndex = index;
    });

    print("$playIndex $index >>> ${snapshot.data.data[index].musicFile}");
    await _audioPlayer.setUrl(snapshot.data.data[index].musicFile);
    // listen for changes in player state
    _audioPlayer.playerStateStream.listen((playerState) async {
      final isPlaying = playerState.playing;
      final processingState = playerState.processingState;
      if (processingState == ProcessingState.loading ||
          processingState == ProcessingState.buffering) {
        buttonNotifier.value = ButtonState.loading;
      } else if (!isPlaying) {
        buttonNotifier.value = ButtonState.paused;
      } else if (processingState != ProcessingState.completed) {
        buttonNotifier.value = ButtonState.playing;
      } else {
        _audioPlayer.seek(Duration.zero);
        _audioPlayer.pause();
        buttonNotifier.value = ButtonState.paused;
        setState(() {
          playIndex = playIndex + 1;
        });
        if (playIndex < snapshot.data.data.length) {
          await init(snapshot, playIndex);
          play();
          print("next");
        }
        print('Complete Song');
      }
    });

    // listen for changes in play position
    _audioPlayer.positionStream.listen((position) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: position,
        buffered: oldState.buffered,
        total: oldState.total,
      );
    });

    // listen for changes in the buffered position
    _audioPlayer.bufferedPositionStream.listen((bufferedPosition) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: oldState.current,
        buffered: bufferedPosition,
        total: oldState.total,
      );
    });

    // listen for changes in the total audio duration
    _audioPlayer.durationStream.listen((totalDuration) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: oldState.current,
        buffered: oldState.buffered,
        total: totalDuration ?? Duration.zero,
      );
    });
  }

  void play() async {
    audioInit = 1;
    _audioPlayer.play();
  }

  void stop() async {
    _audioPlayer.stop();
  }

  void pause() {
    _audioPlayer.pause();
  }

  void seek(Duration position) {
    _audioPlayer.seek(position);
  }

  Map<dynamic, dynamic> mapDuration = Map();

  getDuration(AsyncSnapshot<MusicList> snapshot, int pos) async {
    await apDuration
        .setUrl(snapshot.data.data[pos].musicFile)
        .then((value) async {
      var ss = "${apDuration.duration}";
      var cc = ss.split(":");
      print("${cc[0]} ${cc[1]} ${cc[2].split(".")[0]}");
      var HMS;

      if (int.parse(cc[0]) > 0) {
        HMS = "${cc[0]}:${cc[1]}:${cc[2].split(".")[0]}";
      } else {
        HMS = "${cc[1]}:${cc[2].split(".")[0]}";
      }
      mapDuration[snapshot.data.data[pos].musicFile] = HMS;
      if (pos != snapshot.data.data.length - 1) {
        getDuration(snapshot, pos + 1);
      } else {
        print(mapDuration);
        setState(() {});
      }
    }).catchError((onError) {
      print(" ON ERROR ?? >>  ${onError.toString()}");
    });
  }
}

class ProgressBarState {
  ProgressBarState({
    this.current,
    this.buffered,
    this.total,
  });
  final Duration current;
  final Duration buffered;
  final Duration total;
}

enum ButtonState { paused, playing, loading }

// void init(String url) async {
//   // initialize the song
//   await _audioPlayer.setUrl(url);
//   // listen for changes in player state
//   _audioPlayer.playerStateStream.listen((playerState) {
//     final isPlaying = playerState.playing;
//     final processingState = playerState.processingState;
//     if (processingState == ProcessingState.loading ||
//         processingState == ProcessingState.buffering) {
//       buttonNotifier.value = ButtonState.loading;
//     } else if (!isPlaying) {
//       buttonNotifier.value = ButtonState.paused;
//     } else if (processingState != ProcessingState.completed) {
//       buttonNotifier.value = ButtonState.playing;
//     } else {
//       _audioPlayer.seek(Duration.zero);
//
//       _audioPlayer.pause();
//     }
//   });
//
//   // listen for changes in play position
//   _audioPlayer.positionStream.listen((position) {
//     final oldState = progressNotifier.value;
//     progressNotifier.value = ProgressBarState(
//       current: position,
//       buffered: oldState.buffered,
//       total: oldState.total,
//     );
//   });
//
//   // listen for changes in the buffered position
//   _audioPlayer.bufferedPositionStream.listen((bufferedPosition) {
//     final oldState = progressNotifier.value;
//     progressNotifier.value = ProgressBarState(
//       current: oldState.current,
//       buffered: bufferedPosition,
//       total: oldState.total,
//     );
//   });
//
//   // listen for changes in the total audio duration
//   _audioPlayer.durationStream.listen((totalDuration) {
//     final oldState = progressNotifier.value;
//     progressNotifier.value = ProgressBarState(
//       current: oldState.current,
//       buffered: oldState.buffered,
//       total: totalDuration ?? Duration.zero,
//     );
//   });
// }
