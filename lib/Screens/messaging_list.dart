import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Main/Bloc/videolist-bloc.dart';
import 'package:flutter_app/Model/message_list_model.dart';
import 'package:flutter_app/Providers/video-list-api.dart';
import 'package:flutter_app/Screens/MediatorScreens/mediator_profile_login.dart';
import 'package:flutter_app/Screens/MediatorScreens/user_informaiton_Screen.dart';
import 'package:flutter_app/Screens/UsersScreens/final_message_screen.dart';
import 'package:flutter_app/Screens/cutomise_ad_screen.dart';
import 'package:flutter_app/Screens/home_screen.dart';
import 'package:flutter_app/Screens/splash_screen.dart';
import 'package:flutter_app/Utils/SizeConfig.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/savedatalocal.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:flutter_app/Widget/backgorund.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:selectable_autolink_text/selectable_autolink_text.dart';
import 'package:url_launcher/url_launcher.dart';

class MessagingScreen extends StatefulWidget {
  const MessagingScreen({Key key}) : super(key: key);

  @override
  _MessagingScreenState createState() => _MessagingScreenState();
}

class _MessagingScreenState extends State<MessagingScreen> {
  SlidableController slidableController;
  Animation<double> _rotationAnimation;
  Color _fabColor = Colors.blue;
  Map<String, dynamic> deleteMessageMap = Map();
  List deletedMessages = [];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkResponse(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/back_ic.png'),
        ),
        shadowColor: Colors.transparent,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TextWidgets().boldTextWidget(homeButtonTextColor, Strings.news,
            context, ResponsiveFlutter.of(context).fontSize(3.0)),
      ),
      body: Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 100,
        decoration: backgroundBoxDecoration,
        child: Column(
          children: [
            Expanded(flex: 0, child: CustomiseAdListScreen()),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
                elevation: 1.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(
                        ResponsiveFlutter.of(context).hp(2.0),
                        ResponsiveFlutter.of(context).hp(2.0)))),
                child: StreamBuilder(
                  stream: getVideoListBloc.subMessage.stream,
                  builder: (BuildContext context,
                      AsyncSnapshot<MessageList> snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.data != null
                          ? snapshot.data.data.length != 0
                              ? ListView.builder(
                                  itemCount: snapshot.data.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    // for (int i = 0;
                                    //     i < deletedMessages?.length;
                                    //     i++) {
                                    //   if (snapshot
                                    //           .data.data[index].adminNotificationId ==
                                    //       deletedMessages[i]["id"]) {
                                    //     print(
                                    //         "${snapshot.data.data[index].adminNotificationId} |||| List[$i] : ${deletedMessages[i]["id"]} : Deleted");
                                    //     break;
                                    //   } else {
                                    //     if (i == deletedMessages.length - 1) {
                                    //       print(
                                    //           "${snapshot.data.data[index].adminNotificationId} |||| List[$i] : ${deletedMessages[i]["id"]} : Show");
                                    //     }
                                    //   }
                                    // }

                                    if (deletedMessages.contains(snapshot.data
                                        .data[index].adminNotificationId)) {
                                      return Container();
                                    } else {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  FullMessageScreen(
                                                message: snapshot
                                                    .data
                                                    .data[index]
                                                    .notificationMessage,
                                                navigateHome: false,
                                              ),
                                            ),
                                          );
                                        },
                                        child: Slidable(
                                          direction: Axis.horizontal,
                                          actionExtentRatio: 0.30,
                                          actionPane:
                                              SlidableBehindActionPane(),
                                          // dismissal: SlidableDismissal(
                                          //   child: SlidableDrawerDismissal(),
                                          //   onDismissed: (actionType) {
                                          //     print("dis");
                                          //   },
                                          // ),
                                          key: Key(snapshot.data.data[index]
                                              .adminNotificationId
                                              .toString()),
                                          controller: slidableController,
                                          secondaryActions: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: ResponsiveFlutter.of(
                                                          context)
                                                      .hp(2.5),
                                                  bottom: ResponsiveFlutter.of(
                                                          context)
                                                      .hp(2.5),
                                                  right: ResponsiveFlutter.of(
                                                          context)
                                                      .hp(2.5)),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    ResponsiveFlutter.of(
                                                            context)
                                                        .hp(2.0),
                                                  ),
                                                ),
                                              ),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    ResponsiveFlutter.of(
                                                            context)
                                                        .hp(2.0),
                                                  ),
                                                ),
                                                child: IconSlideAction(
                                                  caption: 'Delete',
                                                  color: wantHelp,
                                                  icon: Icons.delete,
                                                  onTap: () async {
                                                    deleteMessageLocally(snapshot
                                                        .data
                                                        .data[index]
                                                        .adminNotificationId);

                                                    // deleteMessage(snapshot, index);
                                                    // setState(() {
                                                    //   snapshot.data.data
                                                    //       .removeAt(index);
                                                    // });
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                          child: Container(
                                            padding: EdgeInsets.all(
                                                ResponsiveFlutter.of(context)
                                                    .hp(2.5)),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.all(
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .hp(2.5)),
                                                  decoration: BoxDecoration(
                                                    color: videoBackColor,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.elliptical(
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .hp(2.0),
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .hp(2.0),
                                                      ),
                                                    ),
                                                  ),
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      7,
                                                  alignment:
                                                      Alignment.topCenter,
                                                  child: Container(
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceAround,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        // Flexible(
                                                        //   child: Text(
                                                        //     '${snapshot.data.data[index].notificationMessage}',
                                                        //     textAlign:
                                                        //         TextAlign.left,
                                                        //     overflow:
                                                        //         TextOverflow
                                                        //             .ellipsis,
                                                        //     maxLines: 2,
                                                        //     style: TextStyle(
                                                        //       fontSize:
                                                        //           ResponsiveFlutter.of(
                                                        //                   context)
                                                        //               .fontSize(
                                                        //                   1.8),
                                                        //       fontFamily:
                                                        //           fontStyle,
                                                        //       color:
                                                        //           Colors.black,
                                                        //     ),
                                                        //   ),
                                                        // ),
                                                        SelectableAutoLinkText(
                                                            '${snapshot.data.data[index].notificationMessage}',
                                                            textAlign: TextAlign
                                                                .justify,
                                                            maxLines: 2,
                                                            style: TextStyle(
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          1.8),
                                                              fontFamily:
                                                                  fontStyle,
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                            linkStyle:
                                                                TextStyle(
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          1.8),
                                                              fontFamily:
                                                                  fontStyle,
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                            highlightedLinkStyle:
                                                                TextStyle(
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          1.8),
                                                              fontFamily:
                                                                  fontStyle,
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                            onTap: (url) async {
                                                          if (await canLaunch(
                                                              url)) {
                                                            await launch(url);
                                                          } else {
                                                            throw 'Could not launch $url';
                                                          }
                                                        }),
                                                        Container(
                                                          alignment: Alignment
                                                              .bottomRight,
                                                          child: Text(
                                                            StringExtension
                                                                .displayTimeAgoFromTimestamp(
                                                                    "${snapshot.data.data[index].createdDate}"),
                                                            textAlign:
                                                                TextAlign.end,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            maxLines: 1,
                                                            style: TextStyle(
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          1.5),
                                                              fontFamily:
                                                                  fontStyle,
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    }

                                    //=============== Delete below ======== with user id

                                    // return model.user_type == "user"
                                    //     ? snapshot.data.data[index].isUser == "true"
                                    //         ? GestureDetector(
                                    //             onTap: () {
                                    //               Navigator.push(
                                    //                 context,
                                    //                 MaterialPageRoute(
                                    //                   builder: (context) =>
                                    //                       FullMessageScreen(
                                    //                     message: snapshot
                                    //                         .data
                                    //                         .data[index]
                                    //                         .notificationMessage,
                                    //                   ),
                                    //                 ),
                                    //               );
                                    //             },
                                    //             child: Slidable(
                                    //               direction: Axis.horizontal,
                                    //               actionExtentRatio: 0.25,
                                    //               actionPane:
                                    //                   SlidableBehindActionPane(),
                                    //               // dismissal: SlidableDismissal(
                                    //               //   child: SlidableDrawerDismissal(),
                                    //               //   onDismissed: (actionType) {
                                    //               //     print("dis");
                                    //               //   },
                                    //               // ),
                                    //               key: Key(snapshot.data.data[index]
                                    //                   .adminNotificationId
                                    //                   .toString()),
                                    //               controller: slidableController,
                                    //               secondaryActions: [
                                    //                 Container(
                                    //                   margin: EdgeInsets.only(
                                    //                       top: ResponsiveFlutter.of(
                                    //                               context)
                                    //                           .hp(2.5),
                                    //                       bottom:
                                    //                           ResponsiveFlutter.of(
                                    //                                   context)
                                    //                               .hp(2.5),
                                    //                       right: ResponsiveFlutter.of(
                                    //                               context)
                                    //                           .hp(2.5)),
                                    //                   decoration: BoxDecoration(
                                    //                     color: Colors.red,
                                    //                     borderRadius:
                                    //                         BorderRadius.all(
                                    //                       Radius.circular(
                                    //                         ResponsiveFlutter.of(
                                    //                                 context)
                                    //                             .hp(2.0),
                                    //                       ),
                                    //                     ),
                                    //                   ),
                                    //                   child: ClipRRect(
                                    //                     borderRadius:
                                    //                         BorderRadius.all(
                                    //                       Radius.circular(
                                    //                         ResponsiveFlutter.of(
                                    //                                 context)
                                    //                             .hp(2.0),
                                    //                       ),
                                    //                     ),
                                    //                     child: IconSlideAction(
                                    //                       caption: 'Delete',
                                    //                       color: Colors.blue,
                                    //                       icon: Icons.delete,
                                    //                       onTap: () {
                                    //                         deleteMessage(
                                    //                             snapshot, index);
                                    //                       },
                                    //                     ),
                                    //                   ),
                                    //                 ),
                                    //               ],
                                    //               child: Container(
                                    //                 padding: EdgeInsets.all(
                                    //                     ResponsiveFlutter.of(context)
                                    //                         .hp(2.5)),
                                    //                 child: Column(
                                    //                   crossAxisAlignment:
                                    //                       CrossAxisAlignment.start,
                                    //                   children: [
                                    //                     Container(
                                    //                       padding: EdgeInsets.all(
                                    //                           ResponsiveFlutter.of(
                                    //                                   context)
                                    //                               .hp(2.5)),
                                    //                       decoration: BoxDecoration(
                                    //                         color: videoBackColor,
                                    //                         borderRadius:
                                    //                             BorderRadius.all(
                                    //                           Radius.elliptical(
                                    //                             ResponsiveFlutter.of(
                                    //                                     context)
                                    //                                 .hp(2.0),
                                    //                             ResponsiveFlutter.of(
                                    //                                     context)
                                    //                                 .hp(2.0),
                                    //                           ),
                                    //                         ),
                                    //                       ),
                                    //                       height:
                                    //                           MediaQuery.of(context)
                                    //                                   .size
                                    //                                   .height /
                                    //                               7,
                                    //                       alignment:
                                    //                           Alignment.topCenter,
                                    //                       child: Container(
                                    //                         child: Column(
                                    //                           mainAxisAlignment:
                                    //                               MainAxisAlignment
                                    //                                   .spaceAround,
                                    //                           crossAxisAlignment:
                                    //                               CrossAxisAlignment
                                    //                                   .start,
                                    //                           children: [
                                    //                             Flexible(
                                    //                               child: Text(
                                    //                                 '${snapshot.data.data[index].notificationMessage}',
                                    //                                 textAlign:
                                    //                                     TextAlign
                                    //                                         .left,
                                    //                                 overflow:
                                    //                                     TextOverflow
                                    //                                         .ellipsis,
                                    //                                 maxLines: 2,
                                    //                                 style: TextStyle(
                                    //                                   fontSize: ResponsiveFlutter.of(
                                    //                                           context)
                                    //                                       .fontSize(
                                    //                                           1.8),
                                    //                                   fontFamily:
                                    //                                       fontStyle,
                                    //                                   color: Colors
                                    //                                       .black,
                                    //                                 ),
                                    //                               ),
                                    //                             ),
                                    //                             Container(
                                    //                               alignment: Alignment
                                    //                                   .bottomRight,
                                    //                               child: Text(
                                    //                                 StringExtension
                                    //                                     .displayTimeAgoFromTimestamp(
                                    //                                         "${snapshot.data.data[index].createdDate}"),
                                    //                                 textAlign:
                                    //                                     TextAlign.end,
                                    //                                 overflow:
                                    //                                     TextOverflow
                                    //                                         .ellipsis,
                                    //                                 maxLines: 1,
                                    //                                 style: TextStyle(
                                    //                                   fontSize: ResponsiveFlutter.of(
                                    //                                           context)
                                    //                                       .fontSize(
                                    //                                           1.5),
                                    //                                   fontFamily:
                                    //                                       fontStyle,
                                    //                                   color: Colors
                                    //                                       .black,
                                    //                                 ),
                                    //                               ),
                                    //                             ),
                                    //                           ],
                                    //                         ),
                                    //                       ),
                                    //                     ),
                                    //                   ],
                                    //                 ),
                                    //               ),
                                    //             ),
                                    //           )
                                    //         : Container()
                                    //     : snapshot.data.data[index].isMediator ==
                                    //             "true"
                                    //         ? GestureDetector(
                                    //             onTap: () {
                                    //               Navigator.push(
                                    //                 context,
                                    //                 MaterialPageRoute(
                                    //                   builder: (context) =>
                                    //                       FullMessageScreen(
                                    //                     message: snapshot
                                    //                         .data
                                    //                         .data[index]
                                    //                         .notificationMessage,
                                    //                   ),
                                    //                 ),
                                    //               );
                                    //             },
                                    //             child: Slidable(
                                    //               direction: Axis.horizontal,
                                    //               actionExtentRatio: 0.30,
                                    //               actionPane:
                                    //                   SlidableBehindActionPane(),
                                    //               // dismissal: SlidableDismissal(
                                    //               //   child: SlidableDrawerDismissal(),
                                    //               //   onDismissed: (actionType) {
                                    //               //     print("dis");
                                    //               //   },
                                    //               // ),
                                    //               key: Key(snapshot.data.data[index]
                                    //                   .adminNotificationId
                                    //                   .toString()),
                                    //               controller: slidableController,
                                    //               secondaryActions: [
                                    //                 Container(
                                    //                   margin: EdgeInsets.only(
                                    //                       top: ResponsiveFlutter.of(
                                    //                               context)
                                    //                           .hp(2.5),
                                    //                       bottom:
                                    //                           ResponsiveFlutter.of(
                                    //                                   context)
                                    //                               .hp(2.5),
                                    //                       right: ResponsiveFlutter.of(
                                    //                               context)
                                    //                           .hp(2.5)),
                                    //                   decoration: BoxDecoration(
                                    //                     color: Colors.red,
                                    //                     borderRadius:
                                    //                         BorderRadius.all(
                                    //                       Radius.circular(
                                    //                         ResponsiveFlutter.of(
                                    //                                 context)
                                    //                             .hp(2.0),
                                    //                       ),
                                    //                     ),
                                    //                   ),
                                    //                   child: ClipRRect(
                                    //                     borderRadius:
                                    //                         BorderRadius.all(
                                    //                       Radius.circular(
                                    //                         ResponsiveFlutter.of(
                                    //                                 context)
                                    //                             .hp(2.0),
                                    //                       ),
                                    //                     ),
                                    //                     child: IconSlideAction(
                                    //                       caption: 'Delete',
                                    //                       color: Colors.blue,
                                    //                       icon: Icons.delete,
                                    //                       onTap: () {
                                    //                         deleteMessage(
                                    //                             snapshot, index);
                                    //                         setState(() {
                                    //                           snapshot.data.data
                                    //                               .removeAt(index);
                                    //                         });
                                    //                       },
                                    //                     ),
                                    //                   ),
                                    //                 ),
                                    //               ],
                                    //               child: Container(
                                    //                 padding: EdgeInsets.all(
                                    //                     ResponsiveFlutter.of(context)
                                    //                         .hp(2.5)),
                                    //                 child: Column(
                                    //                   crossAxisAlignment:
                                    //                       CrossAxisAlignment.start,
                                    //                   children: [
                                    //                     Container(
                                    //                       padding: EdgeInsets.all(
                                    //                           ResponsiveFlutter.of(
                                    //                                   context)
                                    //                               .hp(2.5)),
                                    //                       decoration: BoxDecoration(
                                    //                         color: videoBackColor,
                                    //                         borderRadius:
                                    //                             BorderRadius.all(
                                    //                           Radius.elliptical(
                                    //                             ResponsiveFlutter.of(
                                    //                                     context)
                                    //                                 .hp(2.0),
                                    //                             ResponsiveFlutter.of(
                                    //                                     context)
                                    //                                 .hp(2.0),
                                    //                           ),
                                    //                         ),
                                    //                       ),
                                    //                       height:
                                    //                           MediaQuery.of(context)
                                    //                                   .size
                                    //                                   .height /
                                    //                               7,
                                    //                       alignment:
                                    //                           Alignment.topCenter,
                                    //                       child: Container(
                                    //                         child: Column(
                                    //                           mainAxisAlignment:
                                    //                               MainAxisAlignment
                                    //                                   .spaceAround,
                                    //                           crossAxisAlignment:
                                    //                               CrossAxisAlignment
                                    //                                   .start,
                                    //                           children: [
                                    //                             Flexible(
                                    //                               child: Text(
                                    //                                 '${snapshot.data.data[index].notificationMessage}',
                                    //                                 textAlign:
                                    //                                     TextAlign
                                    //                                         .left,
                                    //                                 overflow:
                                    //                                     TextOverflow
                                    //                                         .ellipsis,
                                    //                                 maxLines: 2,
                                    //                                 style: TextStyle(
                                    //                                   fontSize: ResponsiveFlutter.of(
                                    //                                           context)
                                    //                                       .fontSize(
                                    //                                           1.8),
                                    //                                   fontFamily:
                                    //                                       fontStyle,
                                    //                                   color: Colors
                                    //                                       .black,
                                    //                                 ),
                                    //                               ),
                                    //                             ),
                                    //                             Container(
                                    //                               alignment: Alignment
                                    //                                   .bottomRight,
                                    //                               child: Text(
                                    //                                 StringExtension
                                    //                                     .displayTimeAgoFromTimestamp(
                                    //                                         "${snapshot.data.data[index].createdDate}"),
                                    //                                 textAlign:
                                    //                                     TextAlign.end,
                                    //                                 overflow:
                                    //                                     TextOverflow
                                    //                                         .ellipsis,
                                    //                                 maxLines: 1,
                                    //                                 style: TextStyle(
                                    //                                   fontSize: ResponsiveFlutter.of(
                                    //                                           context)
                                    //                                       .fontSize(
                                    //                                           1.5),
                                    //                                   fontFamily:
                                    //                                       fontStyle,
                                    //                                   color: Colors
                                    //                                       .black,
                                    //                                 ),
                                    //                               ),
                                    //                             ),
                                    //                           ],
                                    //                         ),
                                    //                       ),
                                    //                     ),
                                    //                   ],
                                    //                 ),
                                    //               ),
                                    //             ),
                                    //           )
                                    //         : Container();
                                  },
                                )
                              : Center(
                                  child: Text(
                                  "No more community news",
                                  style: TextStyle(fontFamily: fontStyle),
                                ))
                          : Container(
                              child: Center(
                                  child: Text(
                                "No more community news",
                                style: TextStyle(fontFamily: fontStyle),
                              )),
                            );
                    } else if (snapshot.hasError)
                      return Center(
                        child: Container(
                          child: Text('community news not available'),
                        ),
                      );
                    else
                      return Center(
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  deleteMessageLocally(id) async {
    deletedMessages.add(id);
    await SaveDataLocal.saveDeleteMessageList(deletedMessages);
    deletedMessages = await SaveDataLocal().getDeleteMessageListFromLocal();
  }

  getInitDeletedList() async {
    deletedMessages = await SaveDataLocal().getDeleteMessageListFromLocal();
    print(deletedMessages);
  }

  @override
  void initState() {
    getInitDeletedList();
    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );
    getVideoListBloc.getMessageList();
    super.initState();
  }

  deleteMessage(AsyncSnapshot<MessageList> snapshot, int index) {
    Map<dynamic, dynamic> bodyData = Map();
    bodyData["user_id"] = "${model.user_id.toString()}";
    bodyData["msg_user_admin_id"] =
        "${snapshot.data.data[index].msgUserAdminId}";

    VideoMusicMessageListAPiClient()
        .messageDeleteAPiClient(bodyData)
        .then((value) {
      if (value.statusCode == 200 ||
          jsonDecode(value.body)["responseCode"] == 3) {
      } else {
        Fluttertoast.showToast(msg: "${jsonDecode(value.body)["message"]}");
      }
    }).catchError((onError) {
      Fluttertoast.showToast(msg: 'Some thing went Wrong.');
    });
  }

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }

  static Widget _getActionPane(int index) {
    switch (index % 4) {
      case 0:
        return SlidableBehindActionPane();
      case 1:
        return SlidableStrechActionPane();
      case 2:
        return SlidableScrollActionPane();
      case 3:
        return SlidableDrawerActionPane();
      default:
        return null;
    }
  }
}

class FullMessageScreen extends StatefulWidget {
  final String message;
  final bool navigateHome;

  const FullMessageScreen({Key key, this.message, this.navigateHome})
      : super(key: key);

  @override
  _FullMessageScreenState createState() => _FullMessageScreenState();
}

class _FullMessageScreenState extends State<FullMessageScreen> {
  String status;
  Map<String, dynamic> mediatorData = new Map<String, dynamic>();

  @override
  void initState() {
    if (widget.navigateHome == true) {
      getUserStatus();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        appBar: AppBar(
          leading: InkResponse(
            onTap: () {
              if (widget.navigateHome == true) {
                navigateToOtherScreen();
              } else {
                Navigator.pop(context);
              }
            },
            child: Image.asset('assets/images/back_ic.png'),
          ),
          shadowColor: Colors.transparent,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: TextWidgets().boldTextWidget(
              homeButtonTextColor,
              'Community News Details',
              context,
              ResponsiveFlutter.of(context).fontSize(3.0)),
        ),
        body: Container(
          width: SizeConfig.safeBlockHorizontal * 100,
          height: SizeConfig.safeBlockVertical * 100,
          decoration: backgroundBoxDecoration,
          child: Card(
            margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
            elevation: 1.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.elliptical(
                    ResponsiveFlutter.of(context).hp(2.0),
                    ResponsiveFlutter.of(context).hp(2.0)))),
            child: Padding(
              padding: EdgeInsets.all(ResponsiveFlutter.of(context).wp(4.0)),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                // child: Text(
                //   widget.message,
                //   textAlign: TextAlign.justify,
                //   style: TextStyle(
                //     fontSize: ResponsiveFlutter.of(context).fontSize(2),
                //     fontFamily: fontStyle,
                //     color: Colors.black,
                //   ),
                // ),
                child: SelectableAutoLinkText(widget.message,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      fontFamily: fontStyle,
                      color: Colors.black,
                    ),
                    linkStyle: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      fontFamily: fontStyle,
                      color: Colors.blue,
                    ),
                    highlightedLinkStyle: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      fontFamily: fontStyle,
                      color: Colors.blue,
                    ), onTap: (url) async {
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                }),
                // child: Linkify(
                //   // softWrap: true,
                //   // strutStyle: StrutStyle(fontSize: 20),
                //   onOpen: (link) async {
                //     if (await canLaunch(link.url)) {
                //       await launch(link.url);
                //     } else {
                //       throw 'Could not launch $link';
                //     }
                //   },
                //   options: LinkifyOptions(
                //     humanize: false,
                //   ),
                //   text: widget.message,
                //   textAlign: TextAlign.justify,
                //   style: TextStyle(
                //     fontSize: ResponsiveFlutter.of(context).fontSize(2),
                //     fontFamily: fontStyle,
                //     color: Colors.black,
                //   ),
                //   linkStyle: TextStyle(
                //     fontSize: ResponsiveFlutter.of(context).fontSize(2),
                //     fontFamily: fontStyle,
                //     color: Colors.blue,
                //   ),
                // ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _willPopCallback() async {
    if (widget.navigateHome == true) {
      navigateToOtherScreen();
    } else {
      Navigator.pop(context);
    }

    return true;
  }

  getUserStatus() async {
    model = await SaveDataLocal.getUserDataFromLocal();
    status = await SaveDataLocal.getUserStatus();
    if (status == 'MediatorPending')
      mediatorData = await SaveDataLocal.getRespondedMediatorType();
    if (status == 'BackUpMediatorPending')
      mediatorData = await SaveDataLocal.getRespondedMediatorType();
  }

  navigateToOtherScreen() {
    if (status == 'UserPending') {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => FinalMessageScreen()),
          (route) => false);
    } else if (status == 'MediatorPending') {
      return Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => UserInformation(
                  mediatorData['isBackUpMediator'] == false
                      ? 'help_request'
                      : 'help_backup_request',
                  mediatorData['helpId'],
                  mediatorData['userId'],
                  0,
                  false)),
          (route) => false);
    } else if (status == 'BackUpMediatorPending') {
      return Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => UserInformation(
                  mediatorData['isBackUpMediator'] == false
                      ? 'help_request'
                      : 'help_backup_request',
                  mediatorData['helpId'],
                  mediatorData['userId'],
                  0,
                  false)),
          (route) => false);
    } else if (model != null) {
      if (model.user_type == 'mediator') {
        return Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => MediatorRegistrationScreen(true, true)),
            (route) => false);
      } else {
        return Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (route) => false);
      }
    } else
      return Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
  }
}

class DeleteMessageModel {
  int id;

  DeleteMessageModel({
    this.id,
  });
}
