import 'package:email_launcher/email_launcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Utils/SizeConfig.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:flutter_app/Widget/backgorund.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUsScreen extends StatefulWidget {
  const ContactUsScreen({Key key}) : super(key: key);

  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController message = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkResponse(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/back_ic.png'),
        ),
        shadowColor: Colors.transparent,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TextWidgets().boldTextWidget(
            homeButtonTextColor,
            Strings.contactUs,
            context,
            ResponsiveFlutter.of(context).fontSize(3.0)),
      ),
      body: Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 100,
        decoration: backgroundBoxDecoration,
        child: Card(
          margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
          elevation: 1.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).hp(2.0)))),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.elliptical(
                ResponsiveFlutter.of(context).hp(2.0),
                ResponsiveFlutter.of(context).hp(2.0))),
            child: Padding(
              padding: EdgeInsets.only(
                  top: ResponsiveFlutter.of(context).wp(2),
                  right: ResponsiveFlutter.of(context).wp(2),
                  left: ResponsiveFlutter.of(context).wp(2)),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          bottom: ResponsiveFlutter.of(context).wp(2)),
                      child: Text(
                        Strings.contactUsDescription,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: ResponsiveFlutter.of(context).fontSize(2),
                          fontFamily: fontStyle,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ResponsiveFlutter.of(context).wp(2.5)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.call,
                            color: sendButtonColor,
                            size: ResponsiveFlutter.of(context).wp(4),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: ResponsiveFlutter.of(context).wp(2.5)),
                            child: GestureDetector(
                              onTap: () {
                                _launchURL("tel://${Strings.mobileNo}");
                              },
                              child: Text(
                                Strings.mobileNo,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  decorationStyle: TextDecorationStyle.solid,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(1.9),
                                  fontFamily: fontStyle,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          bottom: ResponsiveFlutter.of(context).wp(0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.mail,
                            color: sendButtonColor,
                            size: ResponsiveFlutter.of(context).wp(4),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: ResponsiveFlutter.of(context).wp(2.5)),
                            child: GestureDetector(
                              onTap: () {
                                _launchURL('mailto:${Strings.emailContact}');
                              },
                              child: Text(
                                Strings.emailContact,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  decorationStyle: TextDecorationStyle.solid,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(1.9),
                                  fontFamily: fontStyle,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    loginForm(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  final _formKey1 = GlobalKey<FormState>();

  Widget loginForm() {
    return Form(
      key: _formKey1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            flex: 0,
            child: Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.fromLTRB(
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).wp(4.0),
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).wp(2.0)),
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please Enter your name';
                  }
                  return null;
                },
                controller: name,
                style: TextStyle(
                    fontFamily: fontStyle, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  disabledBorder: InputBorder.none,
                  hintText: Strings.name,
                  hintStyle: TextStyle(
                      fontFamily: fontStyle,
                      color: labelColor,
                      fontWeight: FontWeight.w400),
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: ResponsiveFlutter.of(context).wp(2)),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: homeScreenBackgroundColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  border: new OutlineInputBorder(
                    borderSide: BorderSide(
                      color: labelColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                ),
                keyboardType: TextInputType.visiblePassword,
              ),
            ),
          ),
          Flexible(
            flex: 0,
            child: Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.fromLTRB(
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).hp(1.0),
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).wp(2.0)),
              child: TextFormField(
                validator: emailValidator,
                controller: email,
                style: TextStyle(
                    fontFamily: fontStyle, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  hintText: Strings.email,
                  hintStyle: TextStyle(
                      fontFamily: fontStyle,
                      color: labelColor,
                      fontWeight: FontWeight.w400),
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: ResponsiveFlutter.of(context).wp(2)),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: homeScreenBackgroundColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  border: new OutlineInputBorder(
                    borderSide: BorderSide(
                      color: labelColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                ),
                keyboardType: TextInputType.visiblePassword,
              ),
            ),
          ),
          Flexible(
            flex: 0,
            child: Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.fromLTRB(
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).hp(1.0),
                  ResponsiveFlutter.of(context).hp(2.0),
                  ResponsiveFlutter.of(context).wp(2.0)),
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please Enter your message';
                  }
                  return null;
                },
                controller: message,
                maxLines: 4,
                style: TextStyle(
                    fontFamily: fontStyle, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  hintText: "Message",
                  hintStyle: TextStyle(
                      fontFamily: fontStyle,
                      color: labelColor,
                      fontWeight: FontWeight.w400),
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: ResponsiveFlutter.of(context).wp(2)),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: homeScreenBackgroundColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  border: new OutlineInputBorder(
                    borderSide: BorderSide(
                      color: labelColor,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                ),
                keyboardType: TextInputType.visiblePassword,
              ),
            ),
          ),
          Flexible(
            flex: 0,
            child: FlatButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
              child: Container(
                height: ResponsiveFlutter.of(context).wp(15),
                decoration: BoxDecoration(
                    color: sendButtonColor,
                    borderRadius:
                        BorderRadius.all(Radius.elliptical(10.0, 10.0))),
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(
                    ResponsiveFlutter.of(context).hp(2.0),
                    ResponsiveFlutter.of(context).hp(1.0),
                    ResponsiveFlutter.of(context).hp(2.0),
                    0),
                // padding: EdgeInsets.only(top: 50.0, bottom: 50.0),
                child: Text(
                  "Submit Message",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: fontStyle,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5)),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () {
                if (_formKey1.currentState.validate()) {
                  launchEmail();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void launchEmail() async {
    List<String> to = [Strings.emailContact];
    String subject = name.text;
    String compose = message.text;

    Email email = Email(to: to, subject: subject, body: compose);
    await EmailLauncher.launch(email);
  }
}

String emailValidator(val) {
  RegExp emailValid = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  if (!emailValid.hasMatch(val)) {
    return "Invalid email address";
  } else {
    return null;
  }
}

// User => 7434078121 : 123456
// Representative : 123456789 : 123456
//
// Number >> 4242 4242 4242 4242
// Ex Month >> 12
// Ex Year >> 23
// Address Line 1 >> Uttran
// Address Line 2 >> Angel Square
// Address City >> Surat
// Address State >> Gujarat
// Address Country >> India
// Address Zip >> 394105
//flutter: tok_1JkpAYFJhgTQuxAequCOts7c
