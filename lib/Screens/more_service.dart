import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/cutomise_ad_screen.dart';
import 'package:flutter_app/Utils/SizeConfig.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:flutter_app/Widget/backgorund.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';

class MoreService extends StatefulWidget {
  const MoreService({Key key}) : super(key: key);

  @override
  _MoreServiceState createState() => _MoreServiceState();
}

class _MoreServiceState extends State<MoreService> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        leading: InkResponse(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/back_ic.png'),
        ),
        shadowColor: Colors.transparent,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TextWidgets().boldTextWidget(
            homeButtonTextColor,
            Strings.moreService,
            context,
            ResponsiveFlutter.of(context).fontSize(3.0)),
      ),
      body: Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 100,
        decoration: backgroundBoxDecoration,
        child: Column(
          children: [
            Expanded(flex: 0, child: CustomiseAdListScreen()),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
                elevation: 1.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(
                        ResponsiveFlutter.of(context).hp(2.0),
                        ResponsiveFlutter.of(context).hp(2.0)))),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.elliptical(
                      ResponsiveFlutter.of(context).hp(2.0),
                      ResponsiveFlutter.of(context).hp(2.0))),
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).wp(2),
                        right: ResponsiveFlutter.of(context).wp(2),
                        left: ResponsiveFlutter.of(context).wp(2)),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                bottom: ResponsiveFlutter.of(context).wp(2)),
                            child: Container(
                              // height: 50,
                              child: Text(
                                Strings.moreServiceDisclaimerText,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.1),
                                  fontFamily: fontStyle,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                bottom: ResponsiveFlutter.of(context).wp(1.8)),
                            child: Container(
                              // height: 300,
                              child: RichText(
                                textAlign: TextAlign.justify,
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: Strings.pleaseNote,
                                    style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(1.9),
                                      fontFamily: fontStyle,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  TextSpan(
                                    text: Strings
                                        .moreServiceDisclaimerTextDescription,
                                    style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(1.9),
                                      fontFamily: fontStyle,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                            ),
                          ),
                          Container(
                            height: 1030,
                            child: WebViewPlus(
                              javascriptMode: JavascriptMode.unrestricted,
                              onWebViewCreated: (controller) {
                                controller.loadString(r"""
                               <html><div style="height:1000px;" id="uu-container"></div>
<script type="text/javascript" src="https://widgets.uniteus.io/public/widget.js"></script>
<script type="text/javascript">window.Uniteus.assistanceRequestWidget('xH4Px3-uBFfcH4id4DnG_FHtBnlSrudBTeuYfhhi');
</script>
</html>""");
                              },
                              onPageStarted: (url) {
                                print("started $url");
                              },
                              onPageFinished: (url) {
                                print("Finish $url");
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//Lorem Epsom donor sit met, consectrtur adipiscing aliased do eiusmod temper incididunt ut labore et dolore magna liqua. Ul enim ad minim venom,quis nostrum exercitation ullamco la nt, sunt in culpa qui officia deserunt mollit anim id est laboum.
