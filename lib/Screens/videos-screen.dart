import 'package:flutter/material.dart';
import 'package:flutter_app/Main/Bloc/videolist-bloc.dart';
import 'package:flutter_app/Model/video_list.dart';
import 'package:flutter_app/Screens/cutomise_ad_screen.dart';
import 'package:flutter_app/Screens/video_player_view.dart';
import 'package:flutter_app/Utils/SizeConfig.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Widget/backgorund.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideosScreen extends StatefulWidget {
  const VideosScreen({Key key}) : super(key: key);

  @override
  _VideosScreenState createState() => _VideosScreenState();
}

class _VideosScreenState extends State<VideosScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkResponse(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/back_ic.png'),
        ),
        shadowColor: Colors.transparent,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TextWidgets().boldTextWidget(homeButtonTextColor, 'Videos',
            context, ResponsiveFlutter.of(context).fontSize(3.0)),
      ),
      body: Container(
        decoration: backgroundBoxDecoration,
        child: Column(
          children: [
            Expanded(flex: 0, child: CustomiseAdListScreen()),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(ResponsiveFlutter.of(context).hp(2.0)),
                elevation: 1.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(
                        ResponsiveFlutter.of(context).hp(2.0),
                        ResponsiveFlutter.of(context).hp(2.0)))),
                child: Container(
                  child: StreamBuilder<VideoList>(
                      stream: getVideoListBloc.subVideo.stream,
                      builder: (context, AsyncSnapshot<VideoList> snapshot) {
                        if (snapshot.hasData) {
                          snapshot.data.data.sort((a, b) =>
                              DateTime.parse(b.createdDate).toLocal().compareTo(
                                  DateTime.parse(a.createdDate).toLocal()));
                          // lengthOfAudio(snapshot.data.data[0].videoFilesPath);
                          // getDura();
                          return snapshot.data.data != null
                              ? ListView.builder(
                                  itemCount: snapshot.data.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    print(snapshot
                                        .data.data[index].videoFilesPath);
                                    return GestureDetector(
                                      onTap: () async {
                                        // getDura();

                                        convertUrlToIdAndNavigate(
                                            snapshot, 0, index);

                                        // var videoIdd = YoutubePlayer.convertUrlToId(
                                        //     "${snapshot.data.data[index].videoFilesPath}");
                                        //
                                        // await Navigator.push(
                                        //     context,
                                        //     MaterialPageRoute(
                                        //       builder: (context) => YoutubeDefaultWidget(
                                        //         videoId: videoIdd,
                                        //       ),
                                        //     ));
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(
                                          vertical:
                                              ResponsiveFlutter.of(context)
                                                  .scale(5.0),
                                          horizontal:
                                              ResponsiveFlutter.of(context)
                                                  .scale(10.0),
                                        ),
                                        padding: EdgeInsets.all(
                                            ResponsiveFlutter.of(context)
                                                .hp(2.5)),
                                        decoration: BoxDecoration(
                                          color: videoBackColor,
                                          borderRadius: BorderRadius.all(
                                            Radius.elliptical(
                                              ResponsiveFlutter.of(context)
                                                  .hp(2.0),
                                              ResponsiveFlutter.of(context)
                                                  .hp(2.0),
                                            ),
                                          ),
                                        ),
                                        height: ResponsiveFlutter.of(context)
                                            .hp(14),
                                        alignment: Alignment.topCenter,
                                        child: FutureBuilder(
                                          future: getDetail(snapshot.data.data
                                              .elementAt(index)
                                              .videoFilesPath),
                                          builder: (BuildContext context,
                                              AsyncSnapshot<dynamic>
                                                  futureSnapshot) {
                                            if (futureSnapshot.hasData) {
                                              return Row(
                                                children: [
                                                  Flexible(
                                                    flex: 4,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(
                                                            ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(8.0),
                                                          ),
                                                        ),
                                                      ),
                                                      child: Stack(
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .center,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .all(
                                                                Radius.circular(
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .scale(
                                                                          8.0),
                                                                ),
                                                              ),
                                                            ),
                                                            clipBehavior:
                                                                Clip.hardEdge,
                                                            child:
                                                                Image.network(
                                                              futureSnapshot
                                                                      .data[
                                                                  'thumbnail_url'],
                                                              fit: BoxFit.cover,
                                                              width: double
                                                                  .infinity,
                                                            ),
                                                          ),
                                                          Center(
                                                            child: Image.asset(
                                                              'assets/images/play_icon_ic.png',
                                                              scale: 2.5,
                                                            ),
                                                          ),
                                                          // Align(
                                                          //   alignment:
                                                          //       Alignment.bottomRight,
                                                          //   child: Container(
                                                          //     margin: EdgeInsets.all(
                                                          //       ResponsiveFlutter.of(
                                                          //               context)
                                                          //           .scale(5.0),
                                                          //     ),
                                                          //     alignment: Alignment.center,
                                                          //     width: 30,
                                                          //     height: 15,
                                                          //     decoration: BoxDecoration(
                                                          //       color: Colors.white,
                                                          //       borderRadius:
                                                          //           BorderRadius.all(
                                                          //         Radius.circular(
                                                          //           ResponsiveFlutter.of(
                                                          //                   context)
                                                          //               .scale(10.0),
                                                          //         ),
                                                          //       ),
                                                          //     ),
                                                          //     child: Text(
                                                          //       "04:50",
                                                          //       textAlign: TextAlign.center,
                                                          //       overflow:
                                                          //           TextOverflow.ellipsis,
                                                          //       maxLines: 1,
                                                          //       style: TextStyle(
                                                          //         fontSize:
                                                          //             ResponsiveFlutter.of(
                                                          //                     context)
                                                          //                 .fontSize(1.3),
                                                          //         fontFamily: fontStyle,
                                                          //         color: Colors.black,
                                                          //       ),
                                                          //     ),
                                                          //   ),
                                                          // ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Flexible(
                                                    flex: 7,
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          left: ResponsiveFlutter
                                                                  .of(context)
                                                              .hp(2.0)),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceAround,
                                                        children: [
                                                          Container(
                                                            child: Text(
                                                              snapshot.data.data
                                                                  .elementAt(
                                                                      index)
                                                                  .videoTitle,
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              maxLines: 2,
                                                              style: TextStyle(
                                                                fontSize: ResponsiveFlutter.of(
                                                                        context)
                                                                    .fontSize(
                                                                        1.8),
                                                                fontFamily:
                                                                    fontStyle,
                                                                color: Colors
                                                                    .black,
                                                              ),
                                                            ),
                                                          ),
                                                          Flexible(
                                                            child: Container(
                                                              alignment: Alignment
                                                                  .bottomRight,
                                                              child: Text(
                                                                StringExtension
                                                                    .displayTimeAgoFromTimestamp(
                                                                        "${snapshot.data.data[index].createdDate}"),
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                maxLines: 1,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          1.5),
                                                                  fontFamily:
                                                                      fontStyle,
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            } else if (!snapshot.hasData)
                                              return Container(
                                                child: Text(
                                                    'Videos not available'),
                                              );
                                            else
                                              return Container(
                                                child: Center(
                                                    child: Center(
                                                        child:
                                                            CircularProgressIndicator())),
                                              );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                )
                              : Center(
                                  child: Container(
                                    child: Text('Videos not available'),
                                  ),
                                );
                        } else if (snapshot.hasError)
                          return Center(
                            child: Container(
                              child: Text('Videos not available'),
                            ),
                          );
                        else
                          return Center(
                            child: Container(
                              child: CircularProgressIndicator(),
                            ),
                          );
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    getVideoListBloc.getVideoList();
    super.initState();
  }

  convertUrlToIdAndNavigate(
      AsyncSnapshot<VideoList> snapshot, int inx, int tapIndex) async {
    List youtubeIds = [];

    for (int i = 0; i < snapshot.data.data.length; i++) {
      var videoId = YoutubePlayer.convertUrlToId(
          "${snapshot.data.data[i].videoFilesPath}");
      setState(() {
        youtubeIds.add(videoId);
      });
      if (i == snapshot.data.data.length - 1) {
        print(youtubeIds);
        print(youtubeIds[tapIndex]);

        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FlutterYoutubeViewDefault(
                index: tapIndex,
                videoIdList: youtubeIds,
                videoId: youtubeIds[tapIndex],
              ),
            ));

        // if (tapIndex == 2) {
        //   print("iframe Player");
        //
        //   await Navigator.push(
        //       context,
        //       MaterialPageRoute(
        //         builder: (context) => YoutubeIFrame(),
        //       ));
        // }
      }
    }

    // var videoId = YoutubePlayer.convertUrlToId(
    //     "${snapshot.data.data[inx].videoFilesPath}");
    // youtubeIds.add(videoId);
    // setState(() {});
    //
    // if (inx == 2) {
    //   print(youtubeIds);
    //   print(">> ${youtubeIds[tapIndex]}");
    //
    // } else {
    //   convertUrlToIdAndNavigate(snapshot, inx + 1, tapIndex);
    // }
  }

// getDura() async {
//   print('this is >> ' + videoIdd);
//
//   _controller = YoutubePlayerController(
//     initialVideoId: "w4ClQO0FFQg",
//   );
//   print(">> ${_controller.value.metaData}");
//   Future.delayed(Duration(seconds: 60), () {
//     print("60 Sec >> ${_controller.metadata}");
//     print("60 Sec >> ${_controller.value.metaData}");
//   });
// }

// getDuratuion(AsyncSnapshot<MusicList> snapshot, int pos) {
//   ap.setUrl(snapshot.data.data[pos].musicFile).then((value) async {
//     ap.getDuration().then((dur) async {
//       print("Dur $pos >> $dur");
//       Duration duration = new Duration(milliseconds: dur);
//       String HMS;
//       if (duration.inHours > 0) {
//         HMS =
//         "${duration.inHours}:${duration.inMinutes}:${duration.inSeconds}";
//       } else {
//         HMS = "${duration.inMinutes}:${duration.inSeconds}";
//       }
//       mapDuration[snapshot.data.data[pos].musicFile] = HMS;
//       // await ap.dispose();
//       // await ap.release();
//       if (pos != snapshot.data.data.length - 1) {
//         getDuratuion(snapshot, pos + 1);
//       } else {
//         print(mapDuration);
//         ap.stop();
//         ap.release();
//         ap.dispose();
//         setState(() {});
//       }
//     });
//   });
// }

}
