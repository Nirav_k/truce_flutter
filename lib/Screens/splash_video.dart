import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/MediatorScreens/mediator_profile_login.dart';
import 'package:flutter_app/Screens/MediatorScreens/user_informaiton_Screen.dart';
import 'package:flutter_app/Screens/UsersScreens/final_message_screen.dart';
import 'package:flutter_app/Screens/home_screen.dart';
import 'package:flutter_app/Screens/splash_screen.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/savedatalocal.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:video_player/video_player.dart';

class SplashVideo extends StatefulWidget {
  const SplashVideo({Key key}) : super(key: key);

  @override
  _SplashVideoState createState() => _SplashVideoState();
}

class _SplashVideoState extends State<SplashVideo> {
  VideoPlayerController _controller;
  bool seeVideo = false;
  String status;
  Map<String, dynamic> mediatorData = new Map<String, dynamic>();
  @override
  void initState() {
    // TODO: implement initState
    initializeVideo();
    getUserStatus();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF154687),
      body: seeVideo == true
          ? Stack(
              fit: StackFit.expand,
              children: [
                AspectRatio(
                  aspectRatio: 0.5,
                  child: Container(
                      child: _controller.value.initialized
                          ? OverflowBox(
                              // maxWidth: double.infinity,
                              // maxHeight: double.infinity,
                              alignment: Alignment.center,
                              child: FittedBox(
                                // fit: BoxFit.cover,
                                alignment: Alignment.center,
                                child: Container(
                                  width: _controller.value.size.width,
                                  height: _controller.value.size.height,
                                  child: _controller.value.initialized
                                      ? GestureDetector(
                                          onTap: () {
                                            if (_controller.value.isPlaying) {
                                              _controller?.pause();
                                              setState(() {});
                                            } else {
                                              _controller?.play();
                                              setState(() {});
                                            }
                                          },
                                          child: Stack(
                                            fit: StackFit.expand,
                                            children: [
                                              VideoPlayer(_controller),
                                              AnimatedOpacity(
                                                opacity:
                                                    _controller.value.isPlaying
                                                        ? 0
                                                        : 1,
                                                duration:
                                                    Duration(milliseconds: 500),
                                                child: Icon(
                                                  _controller.value.isPlaying
                                                      ? Icons.pause
                                                      : Icons.play_arrow,
                                                  size: 100,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      : CircularProgressIndicator(),
                                ),
                              ),
                            )
                          : CircularProgressIndicator()),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: GestureDetector(
                    onTap: () async {
                      await SaveDataLocal.setIntroVideoLocal(true);
                      navigateToOtherScreen();
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ResponsiveFlutter.of(context).wp(4),
                        vertical: ResponsiveFlutter.of(context).wp(2),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: ResponsiveFlutter.of(context).wp(4),
                        vertical: ResponsiveFlutter.of(context).wp(2),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).wp(4)),
                          color: homeScreenBackgroundColor),
                      child: Text(
                        "SKIP",
                        style: TextStyle(
                          fontSize: ResponsiveFlutter.of(context).fontSize(2),
                          fontFamily: fontStyle,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: GestureDetector(
                    onTap: () async {
                      await SaveDataLocal.setIntroVideoLocal(false);
                      navigateToOtherScreen();
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ResponsiveFlutter.of(context).wp(4),
                        vertical: ResponsiveFlutter.of(context).wp(2),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: ResponsiveFlutter.of(context).wp(4),
                        vertical: ResponsiveFlutter.of(context).wp(2),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).wp(4)),
                          color: homeScreenBackgroundColor),
                      child: Text(
                        "DO NOT SHOW AGAIN",
                        style: TextStyle(
                          fontSize: ResponsiveFlutter.of(context).fontSize(2),
                          fontFamily: fontStyle,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(),
    );
  }

  initializeVideo() {
    _controller = VideoPlayerController.asset("assets/IntroVideo.mp4")
      ..initialize()
          .then((value) => {
                setState(() {
                  _controller?.setLooping(true);
                  _controller?.setVolume(1);
                  _controller?.play();
                  seeVideo = true;
                })
              })
          .catchError((onError) {
        print("On Error Video >> ${onError.toString()}");
      });
  }

  getUserStatus() async {
    model = await SaveDataLocal.getUserDataFromLocal();
    status = await SaveDataLocal.getUserStatus();
    if (status == 'MediatorPending')
      mediatorData = await SaveDataLocal.getRespondedMediatorType();
    if (status == 'BackUpMediatorPending')
      mediatorData = await SaveDataLocal.getRespondedMediatorType();
  }

  navigateToOtherScreen() {
    if (status == 'UserPending')
      return Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => FinalMessageScreen(),
        ),
      );
    else if (status == 'MediatorPending') {
      print('Flutter Philly' + mediatorData['helpId'].toString());
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => UserInformation(
                  mediatorData['isBackUpMediator'] == false
                      ? 'help_request'
                      : 'help_backup_request',
                  mediatorData['helpId'],
                  mediatorData['userId'],
                  0,
                  false)));
    } else if (status == 'BackUpMediatorPending') {
      print('Flutter Philly' + mediatorData['helpId'].toString());
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => UserInformation(
                  mediatorData['isBackUpMediator'] == false
                      ? 'help_request'
                      : 'help_backup_request',
                  mediatorData['helpId'],
                  mediatorData['userId'],
                  0,
                  false)));
    } else if (model != null) {
      if (model.user_type == 'mediator')
        return Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => MediatorRegistrationScreen(true, true),
            ));
      else {
        return Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomeScreen(),
            ));
      }
    } else
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ));
  }
}

//
