import 'package:flutter/material.dart';
import 'package:flutter_app/Utils/const.dart';
import 'package:flutter_app/Utils/strings.dart';
import 'package:flutter_app/Widget/text.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class SurveyFormScreen extends StatefulWidget {
  const SurveyFormScreen({Key key}) : super(key: key);

  @override
  _INTAKEFormScreenState createState() => _INTAKEFormScreenState();
}

class _INTAKEFormScreenState extends State<SurveyFormScreen> {
  final flutterWebviewPlugin = FlutterWebviewPlugin();

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: WebviewScaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Image.asset('assets/images/back_ic.png'),
          ),
          shadowColor: Colors.transparent,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: TextWidgets().boldTextWidget(
              homeButtonTextColor,
              Strings.appName,
              context,
              ResponsiveFlutter.of(context).fontSize(3.0)),
        ),
        url: Uri.encodeFull(
            "https://docs.google.com/forms/d/e/1FAIpQLScwO8ykSgzP6HEe3YSlQ8Gqhq4xnmxDJZpE7jviSDIoLl0nBQ/viewform"),
        clearCookies: false,
        clearCache: false,
        hidden: true,
        appCacheEnabled: false,
        supportMultipleWindows: true,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    flutterWebviewPlugin.dispose();
    super.dispose();
  }
}
