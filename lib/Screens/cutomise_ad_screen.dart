import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Main/Bloc/customise_ad_bloc.dart';
import 'package:flutter_app/Model/custmise_ad_list.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomiseAdListScreen extends StatefulWidget {
  const CustomiseAdListScreen({Key key}) : super(key: key);

  @override
  _CustomiseAdListScreenState createState() => _CustomiseAdListScreenState();
}

class _CustomiseAdListScreenState extends State<CustomiseAdListScreen> {
  final key = new GlobalKey<_CustomiseAdListScreenState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getApiData();
  }

  void getHeight() {
    //returns null:
  }

  getApiData() {
    getCustomiseAdListBloc.getCustomiseAdList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: getCustomiseAdListBloc.subCustomiseAd.stream,
        builder:
            (BuildContext context, AsyncSnapshot<CustomiseAdList> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.data != null
                ? snapshot.data.data.length != 0
                    ? CarouselSlider.builder(
                        key: key,
                        itemCount: snapshot.data.data.length,
                        options: CarouselOptions(
                          autoPlay: true,
                          // height: 50,
                          aspectRatio: 7.5,
                          enlargeCenterPage: true,
                          viewportFraction: 1,
                        ),
                        itemBuilder: (BuildContext context, int index,
                            int pageViewIndex) {
                          return GestureDetector(
                            onTap: () {
                              _launchURL(
                                  "${snapshot.data.data[index].googleAdvertiseUrl}");
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(
                                  ResponsiveFlutter.of(context).wp(1.3)),
                              child: CachedNetworkImage(
                                imageUrl: snapshot
                                    .data.data[index].googleAdvertiseImage,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) => Container(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          );
                        })
                    : Container()
                : Container();
          } else if (snapshot.hasError)
            return Container();
          else
            return Container();
        },
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
