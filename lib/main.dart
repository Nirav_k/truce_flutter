import 'dart:io';
import 'dart:io' show Platform;

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/Screens/splash_screen.dart';
import 'package:flutter_app/Services/voip_push.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'Services/agora.dart';
import 'Services/firebase.dart';
import 'Widget/progrssIndicator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  if (Platform.isIOS) await VoIPPushManager().init();
  await AgoraCallingManager().init();
  // await CallKeep.setup();
  await PushNotificationsManager().init();
  Utils.configLoading();
  // MobileAds.setTestDeviceIds(['9345804C1E5B8F0871DFE29CA0758842']);
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static bool isCallingScreen = false;
  static bool isAnswered = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        navigatorKey: PushNotificationsManager.navigatorKey,
        builder: EasyLoading.init(),
        home: SplashScreen());
  }
}
//
// # Uncomment this line to define a global platform for your project
// platform :ios, '10.0'
//
// # CocoaPods analytics sends network stats synchronously affecting flutter build latency.
// ENV['COCOAPODS_DISABLE_STATS'] = 'true'
//
// project 'Runner', {
//   'Debug' => :debug,
//   'Profile' => :release,
//   'Release' => :release,
// }
//
// def flutter_root
//   generated_xcode_build_settings_path = File.expand_path(File.join('..', 'Flutter', 'Generated.xcconfig'), __FILE__)
//   unless File.exist?(generated_xcode_build_settings_path)
//     raise "#{generated_xcode_build_settings_path} must exist. If you're running pod install manually, make sure flutter pub get is executed first"
//   end
//
//   File.foreach(generated_xcode_build_settings_path) do |line|
//     matches = line.match(/FLUTTER_ROOT\=(.*)/)
//     return matches[1].strip if matches
//   end
//   raise "FLUTTER_ROOT not found in #{generated_xcode_build_settings_path}. Try deleting Generated.xcconfig, then run flutter pub get"
// end
//
// require File.expand_path(File.join('packages', 'flutter_tools', 'bin', 'podhelper'), flutter_root)
//
// flutter_ios_podfile_setup
//
// target 'Runner' do
//   use_frameworks!
//   use_modular_headers!
//
//   flutter_install_all_ios_pods File.dirname(File.realpath(__FILE__))
// end
//
//
// post_install do |installer|
//   installer.pods_project.targets.each do |target|
//     flutter_additional_ios_build_settings(target)
//     target.build_configurations.each do |config|
//           config.build_settings['SWIFT_VERSION'] = '5.0'
//           config.build_settings['ENABLE_BITCODE'] = 'NO'
//           config.build_settings['GCC_PREPROCESSOR_DEFINITIONS'] ||= [
//               '$(inherited)',
//
//               ## dart: PermissionGroup.calendar
//               # 'PERMISSION_EVENTS=1',
//
//               ## dart: PermissionGroup.reminders
//               # 'PERMISSION_REMINDERS=1',
//
//               ## dart: PermissionGroup.contacts
//               'PERMISSION_CONTACTS=1',
//
//               ## dart: PermissionGroup.camera
//               'PERMISSION_CAMERA=1',
//
//               ## dart: PermissionGroup.microphone
//               'PERMISSION_MICROPHONE=1',
//
//               ## dart: PermissionGroup.speech
//               # 'PERMISSION_SPEECH_RECOGNIZER=1',
//
//               ## dart: PermissionGroup.photos
//               'PERMISSION_PHOTOS=1',
//
//               ## dart: [PermissionGroup.location, PermissionGroup.locationAlways, PermissionGroup.locationWhenInUse]
//               'PERMISSION_LOCATION=1',
//
//               ## dart: PermissionGroup.notification
//               'PERMISSION_NOTIFICATIONS=1',
//
//               ## dart: PermissionGroup.mediaLibrary
//               'PERMISSION_MEDIA_LIBRARY=1',
//
//               ## dart: PermissionGroup.sensors
//               # 'PERMISSION_SENSORS=1',
//
//               ## dart: PermissionGroup.bluetooth
//               # 'PERMISSION_BLUETOOTH=1',
//
//               ## dart: PermissionGroup.appTrackingTransparency
//               # 'PERMISSION_APP_TRACKING_TRANSPARENCY=1',
//
//               ## dart: PermissionGroup.criticalAlerts
//               # 'PERMISSION_CRITICAL_ALERTS=1'
//             ]
//         end
//   end
// end
